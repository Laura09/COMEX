import XLSX from 'xlsx'
import socket from '~/plugins/services/socket.js'
export class AxiosService {
  constructor (axios) {
    this.axios = axios
  }

  /**
   * GET ALL FUNCTION
   *
   * @description :: Make call to axios
   * @param :: axios
   * @param :: ULR = Server url
   */
  getAll (url) {
    return new Promise((resolve, reject) => {
      this.axios
        .get(url)
        .then(response => resolve(response.data))
        .catch(err => reject(err.response.data))
    })
  }
  /**
   * GENERAL AXIOS CALL FUNCTION
   *
   * @description :: Make any calls to axios (return success or error)
   * @param :: axios
   * @param :: method = Type of method (POST, GET,PUT, DELETE)
   * @param :: ULR = Server url
   * @param :: data = (Not required)
   */

  generalAxiosCall (method, url, data = null) {
    return new Promise((resolve, reject) => {
      this.axios({
        method,
        url,
        data
      })
        .then((response) => {
          resolve(response.data)
        })
        .catch(err => reject(err.response.data))
    })
  }

  /**
   *  GET ALL ITEMS FROM DB
   *
   * @description :: Get items from BD GET OR POST REQUEST
   * @param :: getItemsObject {type, url, data , notify (notifications alert) }
   */
  async getItems (getItemsObject) {
    const { type, url, data, notify } = getItemsObject
    try {
      return await this.generalAxiosCall(type, url, data)
    } catch (err) {
      if (notify) {
        notify({
          message: err.message,
          type: err.type
        })
      }
      return []
    }
  }

  /**
   * Dowload file from server side
   * @param {*} optionsObject :: { url, data, notify }
   */
  async downloadFile (optionsObject) {
    const { url, data, notify } = optionsObject
    try {
      const response = await this.axios.post(url, data, {
        responseType: 'blob'
      })
      const a = document.createElement('a')
      const link = window.URL.createObjectURL(response.data)
      a.href = link
      a.download = `${data.fileName}.${data.fileExtension}`
      a.click()
      a.remove()
      window.URL.revokeObjectURL(link)
    } catch (err) {
      notify({
        message: 'Error al descargar el archivo',
        type: 'danger'
      })
    }
  }

  /**
   * generateExcelFileAxios
   * @param {*} optionsObject :: { method, url, data, notify, fileName, wsName }
   */
  async generateExcelFileAxios (optionsObject) {
    const { method, url, data, notify, fileName, wsName } = optionsObject
    try {
      const excelRows = await this.paginateInfoPromise({
        data,
        method,
        url
      })
      if (excelRows.length > 0) {
        const wb = XLSX.utils.book_new()
        const ws = XLSX.utils.json_to_sheet(excelRows)
        XLSX.utils.book_append_sheet(wb, ws, wsName)
        XLSX.writeFile(wb, fileName)
      } else {
        notify({
          message: 'Registros no encontrados',
          type: 'warning'
        })
      }
    } catch (err) {
      notify({
        message: err.message,
        type: err.type
      })
    }
  }

  /** PAGINATE INFO WHIT AXIOS PROMISSES */
  paginateInfoPromise (paginateObject) {
    return new Promise((resolve, reject) => {
      ;(async () => {
        const resultResponse = []
        const promiseArray = []
        const { url, data, method } = paginateObject
        try {
          let currentPage = 1
          const response = await this.axios({
            method,
            url,
            data: { ...data, currentPage }
          })
          if (response && response.data.item.length > 0) {
            resultResponse.push(...response.data.item)
            const totalPages = Math.ceil(Number(response.data.totalRows) / data.perPage)
            while (currentPage <= totalPages) {
              currentPage += 1
              promiseArray.push(
                this.axios({
                  method: 'POST',
                  url,
                  data: { ...data, currentPage }
                })
              )
            }
            const resolvedPromises = await Promise.all(promiseArray)
            let i = 0
            while (i < resolvedPromises.length) {
              resultResponse.push(...resolvedPromises[i].data.item)
              i = i + 1
            }
          }
          resolve(resultResponse)
        } catch (err) {
          reject(err)
        }
      })()
    })
  }

  /**
   * showChatRoom
   * @param {*} optionsObject :: {email, roomName }
   * @return chat object
   */
  async showChatRoom (optionsObject) {
    const { email, roomName } = optionsObject
    const chatRoom = await this.generalAxiosCall(
      'GET',
      `/api/chatRoom/${roomName}`
    )
    if (chatRoom) {
      socket.emit('join', {
        chatRoom: chatRoom._id
      })
      socket.emit('join', {
        chatRoom: `${chatRoom._id}${email}`
      })
    }
    return chatRoom
  }
}
