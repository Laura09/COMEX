/* eslint-disable import/no-named-as-default */
import io from 'socket.io-client'
const socket = io(`ws://${process.env.AXIOS_URL}:${process.env.AXIOS_PORT}`)
export default socket
