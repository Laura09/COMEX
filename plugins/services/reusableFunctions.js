import XLSX from 'xlsx'
import moment from 'moment'
import { XMLParser } from 'fast-xml-parser'
import ExcelJS from 'exceljs'
import socket from '~/plugins/services/socket.js'

export class ReusableService {
  /**
   * @description ::  READ EXCEL FILE AND RETURNS A JSON (XLSX FILE)
   * @param {*} excelObject :: {file, headers, options, readCellAddres}
   * @returns JSON
   */
  getExcelJSON (excelObject) {
    const { file, headers, options, readCellAddres } = excelObject
    return new Promise((resolve, reject) => {
      const jsonFile = []
      const fileReader = new FileReader()
      fileReader.readAsBinaryString(file)
      fileReader.onload = (event) => {
        const data = event.target.result
        const workbook = XLSX.read(data, options)
        workbook.SheetNames.forEach((sheet) => {
          if (readCellAddres) { // READ ESPECIFIC CELL
            const worksheet = workbook.Sheets[sheet]
            const desiredCell = worksheet[readCellAddres]
            const cellValue = (desiredCell ? desiredCell.v : undefined)
            if (cellValue) {
              jsonFile.push(cellValue)
            }
          } else { // READ ALL FILE
            const rowObject = XLSX.utils.sheet_to_json(
              workbook.Sheets[sheet],
              headers || ''
            )
            jsonFile.push(...rowObject)
          }
        })
        resolve(jsonFile)
      }
    })
  }

  /**
   * @description ::  READ XML FILE AND RETURNS A JSON
   * @param {*} xmlObject :: {file, headers, options}
   * @returns JSON
   */
  getXMLFile (xmlObject) {
    const { file } = xmlObject
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader()
      fileReader.readAsText(file)
      fileReader.onload = (event) => {
        const data = event.target.result
        const options = {
          ignoreAttributes: false
        }
        const parser = new XMLParser(options)
        resolve(parser.parse(data))
      }
    })
  }

  // REMOVE EMPTY ELEMENTS OF AN OBJECT
  removeEmpty (object) {
    return Object.fromEntries(
      Object.entries(object).filter(([_, v]) => v !== null && v !== undefined)
    )
  }

  // VALIDATE FORMS WITH VEE VALIDATOR
  getValidationState ({ dirty, validated, valid = null }) {
    return dirty || validated ? valid : null
  }

  // ADD ITEMS TO FORMDATA
  formDataAppend (data) {
    const formData = new FormData()
    Object.keys(data).forEach((key) => {
      formData.append(key, data[key] || '')
    })
    return formData
  }

  getDate (date) {
    return moment(date).utc().format('DD-MM-YYYY')
  }

  // performs a trim to the elements of an object
  trimObjects (obj) {
    Object.keys(obj).forEach((k) => {
      if (typeof obj[k] === 'string') {
        obj[k] = obj[k].trim()
      }
    })
    return obj
  }

  // CREATE EXCEL FILE
  /**
   *
   * @param {*} excelObject = {columns, rows, fileName}
   */
  createExcelFile (excelObject) {
    const { columns, rows, fileName, notify } = excelObject
    const workbook = new ExcelJS.Workbook()
    const worksheet = workbook.addWorksheet(fileName)
    worksheet.columns = [...columns]
    worksheet.addRows(rows)
    workbook.xlsx
      .writeBuffer()
      .then((data) => {
        const blob = new Blob([data], {
          type: 'application/octet-binary'
        })
        const url = window.URL.createObjectURL(blob)
        const a = document.createElement('a')
        a.href = url
        a.download = `${fileName}.xlsx`
        a.click()
        a.remove()
      })
      .catch(() => {
        notify({
          message: 'Error al descargar el archivo',
          type: 'danger'
        })
      })
  }

  leaveChats (optionsObject) {
    const { chatRoom, email } = optionsObject
    socket.emit('leave', {
      chatRoom
    })
    socket.emit('leave', {
      chatRoom: `${chatRoom}${email}`
    })
  }
}
