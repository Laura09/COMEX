import { ReusableService } from './reusableFunctions'
import { AxiosService } from './axios'
export default ({ $axios }, inject) => {
  const axios = new AxiosService($axios)
  const service = new ReusableService(axios)
  inject('service', service)
  inject('axiosService', axios)
}
