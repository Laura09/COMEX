import Vue from 'vue'
// ====================== DIRECTIVE =======================================
import clickOutside from './directives/click-ouside.js'

// =======================PLUGINS ==========================================
import './plugins/veeValidate'
import './plugins/sweetalert'
import './plugins/vselect'
// ====================== COMPONENTS PLUGINS ===============================
// Sidebar on the right. Used as a local plugin in DashboardLayout.vue
import SideBar from '~/components/plugins/SidebarPlugin'
// Notifications plugin. Used on Notifications page
import Notifications from '~/components/plugins/NotificationPlugin'
// ====================== GLOBAL BASE COMPONENTS ============================
import BaseDropdown from '~/components/base/DropdownComponent.vue'
import BaseButton from '~/components/base/ButtonComponent.vue'
import Card from '~/components/base/cards/CardComponent.vue'
import BaseInput from '~/components/base/Inputs/InputComponent.vue'
import BaseCheckbox from '~/components/base/Inputs/CheckboxComponent.vue'

Vue.use(SideBar)
Vue.use(Notifications)
Vue.directive('click-outside', clickOutside)
Vue.component(BaseInput.name, BaseInput)
Vue.component(BaseCheckbox.name, BaseCheckbox)
Vue.component(BaseDropdown.name, BaseDropdown)
Vue.component(BaseButton.name, BaseButton)
Vue.component(Card.name, Card)
