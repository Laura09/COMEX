import socketIO from 'socket.io'

export default function () {
  this.nuxt.hook('listen', (server, { host, port }) => {
    const io = socketIO(server, { cors: `http://${host}:${port}` })
    io.on('connection', (socket) => {
      console.log(
        'Client connected',
        `ListentHook: Listening on http://${host}:${port}`
      )
      socket.on('disconnect', () => {
        console.log('Disconnected: ' + socket)
      })

      socket.on('join', function (data) {
        socket.join(data.chatRoom) // We are using room of socket io
      })
      socket.on('save-message', (data) => {
        io.sockets.to(data.chatRoom).emit('new-message', { message: data })
      })
      socket.on('leave', function (data) {
        socket.leave(data.chatRoom) // We are using room of socket io
      })
      socket.on('private-message', ({ message, to }) => {
        to.forEach((email) => {
          io.sockets.to(email).emit('private-message', {
            message
          })
        })
      })
    })

    io.on('connect_error', (err) => {
      console.log(`connect_error due to ${err},`)
    })
  })
}
