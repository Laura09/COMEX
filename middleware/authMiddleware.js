export default ({ $auth, redirect, route }) => {
  const path = route.path
  // object for redirection
  if (!$auth.loggedIn) {
    if (path !== '/auth/loginPage') {
      redirect('/auth/loginPage')
    }
  }
}
