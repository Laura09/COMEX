# comex

## DESCRIPTION

COMEX is a system developed with the NUXTJS, NODEJS EXPRESS and MONGODB tools, it manages and stores a set of highly relevant documents: invoices, packing lists, among others.

## Table of Contents

* [Build Setup](#buil-setup)
* [File Structure](#file-structure)
* [Special Directories](#special-directories)
* [Licensing](#licensing)


## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## File Structure

Within the download you'll find the following directories and files:

```
|-- COMEX
    |-- CHANGELOG.md
    |-- README.md
    |-- app.html
    |-- config.js
    |-- jsconfig.json
    |-- nuxt.config.js
    |-- package.json
    |-- assets
    |   |-- README.md
    |   |-- css
    |   |   |-- demo.css
    |   |   |-- nucleo-icons.css
    |   |-- fonts
    |   |   |-- nucleo.eot
    |   |   |-- nucleo.ttf
    |   |   |-- nucleo.woff
    |   |   |-- nucleo.woff2
    |   |-- sass
    |       |-- black-dashboard.scss
    |       |-- dashboard
    |       |-- vendor
    |           |-- bootstrap-rtl.scss
    |-- components
    |   |-- Base
    |   |   |-- Cards
    |   |   |   |-- CardComponent.vue
    |   |   |-- Inputs
    |   |   |   |-- CheckboxComponent.vue
    |   |   |   |-- IconCheckboxComponent.vue
    |   |   |   |-- RadioComponent.vue
    |   |   |   |-- InputComponent.vue
    |   |   |-- Master
    |   |   |   |-- MasterComponent.vue
    |   |   |   |-- TableComponent.vue
    |   |   |-- AlertComponent
    |   |   |-- ButtonComponent
    |   |   |-- ChatComponent
    |   |   |-- DropdownComponent
    |   |   |-- NavComponent
    |   |   |-- ModalComponent
    |   |   |-- SelectComponent
    |   |-- Core
    |   |   |-- ContentComponent
    |   |   |-- FooterComponent
    |   |   |-- NavbarComponent
    |   |-- Plugins
    |   |   |-- NotificationPlugin
    |   |   |-- SidebarPlugin
    |-- layouts
    |   |-- dashboard.vue
    |   |-- default.vue
    |   |-- error.vue
    |-- io
    |-- middleware
    |   |-- authMiddleware.js
    |   |-- README.md
    |-- pages
    |   |-- Auth
    |   |   |-- loginPage.vue
    |   |-- Dashboard
    |   |   |-- index.vue
    |   |-- ImportModule
    |   |   |-- importDetail.vue
    |   |   |-- importDetail.vue
    |   |   |-- supplyModule.vue
    |   |   |-- uncreatedSkuModule.vue
    |   |   |-- warehouseModule.vue
    |   |-- Management
    |   |   |-- brandPage.vue
    |   |   |-- chatRoomPage2.vue
    |   |   |-- countryPage.vue
    |   |   |-- emailPage.vue
    |   |   |-- exportStatusPage.vue
    |   |   |-- orderStatusPage.vue
    |   |   |-- referencePage.vue
    |   |   |-- templatePage.vue
    |   |   |-- userAccessPage.vue
    |   |   |-- privilegePage.vue
    |   |   |-- rolePage.vue
    |   |   |-- sesionPage.vue
    |-- plugins
    |   |-- services
    |   |   |-- axios.js
    |   |   |-- reusableFuntions.js
    |   |   |-- services.js
    |   |-- template
    |   |   |-- Directives
    |   |   |   |-- click-ouside.js
    |   |-- Plugins
    |   |   |-- sweetalert.js
    |   |   |-- veeValidate.js
    |   |-- dashboard-plugin.js
    |-- static
    |   |-- README.md
    |   |-- comex.png
    |   |-- sw.js
    |   |-- img
    |-- store
    |   |-- README.md
    |-- util
        |-- throttle.js
    |-- server
        |-- config
        |-- controllers
        |-- middlewares
        |-- models
        |-- services
```

## Special Directories

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).


### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).


## Licensing

- Copyright 2022 2BECOMMERCE
