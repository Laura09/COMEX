// grab the things we need
const mongoose = require('mongoose')
const Schema = mongoose.Schema

// create a schema
const roleSchema = new Schema(
  {
    role: { type: String, required: true, unique: true },
    rolePrivileges: [],
    status: { type: String, required: true, default: 'Active' }
  },
  { toJSON: { virtuals: true }, toObject: { virtuals: true }, timestamps: true }
)

roleSchema.virtual('privileges', {
  ref: 'Privilege', // The model to use
  localField: '_id', // Find people where `localField`
  foreignField: 'role._id', // is equal to `foreignField`
  // If `justOne` is true, 'members' will be a single doc as opposed to
  // an array. `justOne` is false by default.
  justOne: false
})
const Role = mongoose.model('Role', roleSchema)

// make this available to our users in our Node applications
module.exports = Role
