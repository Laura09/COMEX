// grab the things we need
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const dbService = require('../services/dbService')
const SessionModel = require('./sessionModel')
const ExportModel = require('./exportModel')

// create a schema
const fileSchema = new Schema(
  {
    exportID: {
      type: Schema.Types.ObjectId,
      ref: 'Export',
      index: true
    },
    brand: {
      type: Schema.Types.ObjectId,
      ref: 'Reference',
      index: true
    },
    country: {
      type: Schema.Types.ObjectId,
      ref: 'Reference',
      index: true,
      required: true
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      index: true
    },
    invoiceNumber: {
      type: String,
      required: true
    },
    fileName: {
      type: String
    },
    fileExtension: {
      type: String
    },
    items: [],
    type: {
      type: String,
      required: true
    },
    packingListType: {
      type: String
    },
    fileId: {
      type: String,
      required: true
    },
    fileDate: {
      type: Date,
      index: true
    },
    status: {
      type: String,
      required: true,
      default: 'Active'
    },
    note: { type: String },
    comexNote: { type: String },
    comexFile: { type: Boolean }
  },
  {
    timestamps: true
  }
)

fileSchema.post('save', function (doc) {
  if (doc.type === 'PL') {
    ExportModel.updateOne(
      { _id: doc.exportID },
      { orderStatus: 'Pendiente Aprobación', status: 'Cargado' }
    ).catch((err) => {
      const session = new SessionModel({
        email: 'Modificación automática',
        action: `Error al modificar de estatus la exportación ${err}`,
        error: true,
        collectionName: 'Export Model'
      })
      dbService.createSesion(session)
    })
  }
})

fileSchema.pre('deleteOne', async function () {
  const deleteModel = await this.model.findOne(this.getQuery())
  if (deleteModel.type === 'PL') {
    ExportModel.updateOne(
      { _id: deleteModel.exportID },
      { orderStatus: 'Pendiente Aprobación', status: 'Cargado' }
    ).catch((err) => {
      const session = new SessionModel({
        email: 'Modificación automática',
        action: `Error al modificar de estatus la exportación ${err}`,
        error: true,
        collectionName: 'Export Model'
      })
      dbService.createSesion(session)
    })
  }
})
const File = mongoose.model('File', fileSchema)

// make this available to our users in our Node applications
module.exports = File
