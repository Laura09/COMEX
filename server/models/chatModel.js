const mongoose = require('mongoose')
const Schema = mongoose.Schema

// create a schema
const chatSchema = new Schema(
  {
    chatRoom: {
      type: mongoose.Schema.Types.ObjectId,
      required: 'Chatroom is required!',
      ref: 'Chatroom'
    },
    to: [],
    email: {
      type: String
    },
    message: {
      type: String
    }
  },
  {
    toJSON: {
      virtuals: true
    },
    toObject: {
      virtuals: true
    },
    timestamps: true
  }
)

const Chat = mongoose.model('Chat', chatSchema)

// make this available to our users in our Node applications
module.exports = Chat
