const mongoose = require('mongoose')
const Schema = mongoose.Schema

// create a schema
const chatRoomSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    }
  },
  {
    toJSON: {
      virtuals: true
    },
    toObject: {
      virtuals: true
    },
    timestamps: true
  }
)

const ChatRoom = mongoose.model('ChatRoom', chatRoomSchema)

// make this available to our users in our Node applications
module.exports = ChatRoom
