// grab the things we need
const mongoose = require('mongoose')
const dbService = require('../services/dbService')
const Session = require('./sessionModel')
const File = require('./fileModel')
const Schema = mongoose.Schema

// create a schema
const referenceSchema = new Schema(
  {
    reference: {
      type: String,
      required: true
    }, // Object Name
    code: {
      type: String
    }, // code or abreviation used for the object
    type: {
      type: String,
      required: true
    }, // Determines the type of object (example: brand, country)
    fileName: {
      // stores the file name
      type: String
    },
    status: {
      type: String,
      required: true,
      default: 'Active'
    }
  },
  {
    toJSON: {
      virtuals: true
    },
    toObject: {
      virtuals: true
    },
    timestamps: true
  }
)

referenceSchema.pre('updateOne', async function (doc) {
  try {
    const updatedDoc = this.getUpdate()
    if (updatedDoc.type === 'reference') {
      const updateBulk = [
        {
          updateMany: {
            filter: {
              'items.reference': updatedDoc.reference,
              type: 'Fact'
            },
            update: {
              $set: {
                'items.$.sku': updatedDoc.code,
                'items.$.incorrect': false
              }
            },
            upsert: false
          }
        },
        {
          updateMany: {
            filter: {
              'items.reference': updatedDoc.reference,
              'items.sku': updatedDoc.code,
              'items.incorrect': false,
              type: 'PL'
            },
            update: {
              $set: {
                'items.$.wrongReference': false
              }
            },
            upsert: false
          }
        }
      ]
      await File.collection.bulkWrite(updateBulk)
    }
  } catch (err) {
    const session = new Session({
      email: 'Modificación automática',
      action: `Error al modificar facturas con sku sin crear ${err.message}`,
      error: true,
      collectionName: 'File Model'
    })
    dbService.createSesion(session)
  }
})

referenceSchema.pre('deleteOne', async function (doc) {
  try {
    const updatedDoc = await this.model.findOne(this.getQuery())
    if (updatedDoc.type === 'reference') {
      const updateBulk = [
        {
          updateMany: {
            filter: {
              'items.reference': updatedDoc.reference,
              type: 'Fact'
            },
            update: {
              $set: {
                'items.$.sku': 'SIN CREAR',
                'items.$.incorrect': true
              }
            },
            upsert: false
          }
        },
        {
          updateMany: {
            filter: {
              'items.reference': updatedDoc.reference,
              type: 'PL'
            },
            update: {
              $set: {
                'items.$.wrongReference': true
              }
            },
            upsert: false
          }
        }
      ]
      await File.collection.bulkWrite(updateBulk)
    }
  } catch (err) {
    const session = new Session({
      email: 'Modificación automática',
      action: `Error al modificar facturas con sku sin crear ${err.message}`,
      error: true,
      collectionName: 'File Model'
    })
    dbService.createSesion(session)
  }
})

referenceSchema.pre('deleteMany', async function () {
  try {
    const deletedData = await this.model.find(this._conditions).lean()
    if (deletedData && deletedData.length > 0) {
      if (deletedData[0].type === 'reference') {
        const bulkOps = []
        deletedData.forEach((item) => {
          const upsertDoc = [
            {
              updateMany: {
                filter: {
                  'items.reference': item.reference,
                  type: 'Fact'
                },
                update: {
                  $set: {
                    'items.$.sku': 'SIN CREAR',
                    'items.$.incorrect': true
                  }
                },
                upsert: true
              }
            },
            {
              updateMany: {
                filter: {
                  'items.reference': item.reference,
                  type: 'PL'
                },
                update: {
                  $set: {
                    'items.$.wrongReference': true
                  }
                },
                upsert: false
              }
            }
          ]

          bulkOps.push(...upsertDoc)
        })
        await File.collection.bulkWrite(bulkOps)
      }
    }
  } catch (err) {
    const session = new Session({
      email: 'Modificación automática',
      action: `Error al modificar facturas con sku sin crear ${err.message}`,
      error: true,
      collectionName: 'File Model'
    })
    dbService.createSesion(session)
  }
})
const Reference = mongoose.model('Reference', referenceSchema)

// make this available to our users in our Node applications
module.exports = Reference
