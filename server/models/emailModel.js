// grab the things we need
const mongoose = require('mongoose')
const Schema = mongoose.Schema

// create a schema
const emailSchema = new Schema({
  type: {
    type: String,
    required: true,
    unique: true,
    index: true
  },
  status: {
    type: String,
    required: true,
    default: 'Active'
  },
  emails: []
}, { timestamps: true })

const Email = mongoose.model('Email', emailSchema)

module.exports = Email
