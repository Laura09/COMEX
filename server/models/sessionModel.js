const mongoose = require('mongoose')
const Schema = mongoose.Schema
const sessionSchema = new Schema(
  {
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    collectionName: { type: String, required: true },
    email: { type: String, required: true },
    action: { type: String, required: true },
    error: { type: Boolean },
    status: { type: String, required: true, default: 'Active' }
  },
  { toJSON: { virtuals: true }, toObject: { virtuals: true }, timestamps: true }
)

const Session = mongoose.model('Session', sessionSchema)
module.exports = Session
