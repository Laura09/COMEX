// grab the things we need
const mongoose = require('mongoose')
const Schema = mongoose.Schema

// create a schema
const templateSchema = new Schema({
  title: {
    type: String,
    required: true,
    unique: true,
    index: true
  },
  header: [],
  columns: [],
  translateColumns: [], // CORRECT NAME OF COLUMNS IN APP
  dataType: [], // CORRECT DATA TYPE
  exampleRow: [],
  fileName: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true,
    default: 'Active'
  }
}, {
  timestamps: true
})

const Template = mongoose.model('Template', templateSchema)

module.exports = Template
