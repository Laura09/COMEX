// grab the things we need
const mongoose = require('mongoose')
const Schema = mongoose.Schema
// create a schema
const exportSchema = new Schema(
  {
    originCountry: {
      type: Schema.Types.ObjectId,
      ref: 'Reference',
      index: true,
      required: true
    },
    destinationCountry: {
      type: Schema.Types.ObjectId,
      ref: 'Reference',
      index: true,
      required: true
    },
    brand: {
      type: Schema.Types.ObjectId,
      ref: 'Reference',
      index: true
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      index: true
    },
    exportDate: {
      type: Date,
      required: true,
      index: true
    },
    exportProcessDate: {
      type: Date,
      index: true
    },
    exportNumber: {
      type: String,
      required: true,
      index: true,
      unique: true
    },
    status: {
      type: String,
      required: true,
      default: 'Cargado'
    },
    orderStatus: {
      type: String,
      required: true,
      default: 'Pendiente Aprobación'
    },
    note: { type: String },
    comexNote: { type: String },
    warehouseNote: { type: String },
    receivedQuantity: { type: Number }
  },
  {
    timestamps: true
  }
)

const Export = mongoose.model('Export', exportSchema)

// make this available to our users in our Node applications
module.exports = Export
