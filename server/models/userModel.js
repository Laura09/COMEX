const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs')

// create a user schema
const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String
  },
  role: {
    type: Schema.Types.ObjectId,
    ref: 'Role'
  },
  brand: [{
    type: Schema.Types.ObjectId,
    ref: 'Reference'
  }],
  country: [{
    type: Schema.Types.ObjectId,
    ref: 'Reference'
  }],
  resetPasswordExpires: Date,
  resetPasswordToken: {
    type: String
  },
  status: {
    type: String,
    required: true,
    default: 'Active'
  }
}, {
  toJSON: {
    virtuals: true
  },
  toObject: {
    virtuals: true
  },
  timestamps: true
})

// generating a hash
userSchema.methods.generateHash = (password) => {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8))
}
// checking if password is valid
userSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password)
}

// delete password
userSchema.methods.toJson = function () {
  const user = this
  const userObject = user.toObject()
  delete userObject.password
  return userObject
}

const User = mongoose.model('User', userSchema)

// make this available to our users in our Node applications
module.exports = User
