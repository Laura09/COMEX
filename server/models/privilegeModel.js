// grab the things we need
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const dbService = require('../services/dbService')
const SessionModel = require('./sessionModel')

// create a schema
const privilegeSchema = new Schema(
  {
    privilege: { type: String, required: true, index: true },
    path: { type: String },
    icon: { type: String },
    parent: {
      type: Schema.Types.ObjectId,
      ref: 'Privilege'
    },
    role: [
      {
        _id: { type: Schema.Types.ObjectId, ref: 'Role', index: true },
        role: { type: String }
      }
    ],
    children: [
      {
        _id: { type: Schema.Types.ObjectId, ref: 'Privilege', index: true }
      }
    ],
    position: { type: Number },
    status: { type: String, required: true, default: 'Active' }
  },
  { timestamps: true }
)

privilegeSchema.post('save', function (doc) {
  if (doc.parent) {
    Privilege.updateOne(
      { _id: doc.parent },
      {
        $addToSet: { children: { _id: doc._id } }
      },
      { upsert: false }
    ).catch((err) => {
      const session = new SessionModel({
        email: 'Modificación automática',
        action: `Error al modificar privilegios de roles ${err}`,
        error: true,
        collectionName: 'Privilege Model'
      })
      dbService.createSesion(session)
    })
  }
})

privilegeSchema.post('deleteOne', async function () {
  const privilegeID = this.getQuery()._id
  await this.model
    .updateOne(
      { 'children._id': privilegeID },
      {
        $pull: { children: { _id: privilegeID } }
      },
      { upsert: false }
    )
    .catch((err) => {
      const session = new SessionModel({
        email: 'Modificación automática',
        action: `Error al modificar privilegios de roles ${err}`,
        error: true,
        collectionName: 'Privilege Model'
      })
      dbService.createSesion(session)
    })
})

const Privilege = mongoose.model('Privilege', privilegeSchema)

// make this available to our users in our Node applications
module.exports = Privilege
