const mongoose = require('mongoose')
module.exports = {
  connection: () => {
    mongoose
      .connect(process.env.URLDB, {
        keepAlive: true,
        keepAliveInitialDelay: 300000,
        connectTimeoutMS: 30000,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
      })
      .then(() => console.log('SUCCESS CONNECTION'))
      .catch(err => console.log('ERROR CONNECTION', err))
  }
}
