const externalService = {}
const axios = require('axios')

// `http://casos-amz.2becommerce.com/externalRequest/products`
/**
 *
 * @param {*} productsObject :: { url, code, country }
 * @returns array of items
 */
externalService.getPaginateInfo = async (productsObject) => {
  const limit = 1000
  let resultResponse = []
  const promiseArray = []
  const { url, code, country } = productsObject
  try {
    let page = 1
    const response = await axios({
      method: 'POST',
      url,
      data: {
        page,
        code,
        country
      }
    })
    if (response && response.data.data.length > 0) {
      resultResponse.push(...response.data.data)
      const totalPages = Math.ceil(Number(response.data.totalRows) / limit)
      page += 1

      while (page <= totalPages) {
        promiseArray.push(
          axios({
            method: 'POST',
            url,
            data: {
              page,
              code,
              country
            }
          })
        )
        page += 1
      }
      const resolvedPromises = await Promise.all(promiseArray)
      let i = 0
      while (i < resolvedPromises.length) {
        resultResponse.push(...resolvedPromises[i].data.data)
        i = i + 1
      }
    }
  } catch (err) {
    resultResponse = []
  }
  return resultResponse
}

module.exports = externalService
