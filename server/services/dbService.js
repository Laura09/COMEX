const fs = require('fs')
const axios = require('axios')
const Session = require('../models/sessionModel')
const responseService = require('./responseService')
const generalFunctions = {}

/**
 * CREATE ONE
 *
 * @param {*} Model
 * @param {*} data
 * @param {*} res
 * @description :: Insert one document in DB
 */
generalFunctions.createOne = (Model, data, res) => {
  Model.create(data)
    .then(() => {
      responseService.resultResponse({
        type: 'save',
        status: 201,
        res
      })
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

/**
 * CREATE ONE
 *
 * @param {*} Model
 * @param {*} collectionName
 * @param {*} data
 * @param {*} req
 * @param {*} res
 * @param {*} returnData
 * @description :: Insert one document in DB
 */
generalFunctions.createOneSession = (Model, collectionName, data, req, res, returnData = false) => {
  const user = req.user
  Model.create(data)
    .then((createdData) => {
      const session = new Session({
        action: 'Creación de nuevo registro',
        collectionName,
        email: user.email,
        user
      })
      generalFunctions.createSesion(session)
      responseService.resultResponse({
        type: 'save',
        status: 201,
        ...returnData ? { data: { ...data, _id: createdData._id } } : '',
        res
      })
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

/**
 *createWithImage
 * @param {*} Model :: Mongoose Model
 * @param {*} collectionName :: Name of Colection
 * @param {*} data :: data to save
 * @param {*} fileData :: object of file
 * @param {*} req
 * @param {*} res
 */
generalFunctions.createWithImage = (
  Model,
  collectionName,
  data,
  fileData,
  req,
  res
) => {
  const user = req.user
  Model.create(data)
    .then(() => {
      const session = new Session({
        action: 'Creación de nuevo registro',
        collectionName,
        email: user.email,
        user
      })
      generalFunctions.createSesion(session)
      generalFunctions.saveFile({
        ...fileData,
        res,
        response: true
      })
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

/**
 * updateWithImage
 * @param {*} Model :: Mongoose Model
 * @param {*} collectionName :: Name of Colection
 * @param {*} params :: Update params
 * @param {*} data :: data to save
 * @param {*} fileData :: object of file
 * @param {*} req
 * @param {*} res
 */
generalFunctions.updateWithImage = (
  Model,
  collectionName,
  params,
  data,
  fileData,
  req,
  res
) => {
  const user = req.user
  Model.updateOne(params, data)
    .then((modelSave) => {
      if (modelSave.nModified > 0) {
        const session = new Session({
          action: 'Actualización de registro',
          collectionName,
          email: user.email,
          user
        })
        generalFunctions.createSesion(session)
        generalFunctions.saveFile({
          ...fileData,
          res,
          response: true
        })
      } else {
        const data = {
          message: 'Ha ocurrido un error al guardar los cambios en base de datos'
        }
        responseService.resultResponse({
          type: 'error',
          status: 400,
          data,
          res
        })
      }
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

/**
 * CREATE ONE Custom Message
 *
 * @param {*} Model
 * @param {*} data
 * @param {*} message
 * @param {*} res
 * @description :: Insert one document in DB
 */
generalFunctions.createOneCustomMessage = (Model, data, message, res) => {
  Model.create(data)
    .then(() => {
      const senData = {
        message,
        type: 'success'
      }
      responseService.resultResponse({
        type: 'custom',
        data: senData,
        status: 201,
        res
      })
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}
/**
 * CREATE MANY
 *
 * @param {*} Model
 * @param {*} data
 * @param {*} res
 * @description :: Insert many documents in DB
 */
generalFunctions.createMany = (Model, data, res) => {
  Model.insertMany(data)
    .then(() => {
      responseService.resultResponse({
        type: 'save',
        status: 201,
        res
      })
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

/**
 * CREATE MANY
 *
 * @description :: Insert many documents in DB
 * @param createObbject :: { model, data, collectionName, action, message, user, res }
 */

generalFunctions.createManyCustomMessageSession = (createObbject) => {
  const { model, data, collectionName, action, message, user, res } = createObbject
  model.insertMany(data)
    .then(() => {
      const session = new Session({
        email: user.email,
        action,
        collectionName,
        user
      })
      generalFunctions.createSesion(session)
      const senData = {
        message,
        type: 'success'
      }
      responseService.resultResponse({
        type: 'custom',
        data: senData,
        status: 201,
        res
      })
    })
    .catch(() => {
      const senData = {
        message,
        type: 'error'
      }
      responseService.resultResponse({
        type: 'custom',
        data: senData,
        status: 500,
        res
      })
    })
}
/**
 * UPDATE ONE
 *
 * @param {*} Model
 * @param {*} params
 * @param {*} data
 * @param {*} res
 * @description :: Update one document from DB
 */
generalFunctions.updateOne = (Model, params, data, res) => {
  Model.updateOne(params, data)
    .then((modelSave) => {
      if (modelSave.nModified > 0) {
        responseService.resultResponse({
          type: 'update',
          status: 200,
          res
        })
      } else {
        const err = {
          message: 'Ha ocurrido un error al guardar los cambios en base de datos'
        }
        responseService.resultResponse({
          type: 'error',
          status: 400,
          data: err,
          res
        })
      }
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}
/**
 * UPSERT ONE
 *
 * @param {*} Model
 * @param {*} collectionName
 * @param {*} params
 * @param {*} data
 * @param {*} req
 * @param {*} res
 * @description :: Upsert one document from DB
 */
generalFunctions.upsertOneSession = (
  Model,
  collectionName,
  params,
  data,
  req,
  res
) => {
  const user = req.user
  Model.updateOne(params, data, {
    upsert: true
  })
    .then((modelSave) => {
      if (modelSave.nModified > 0 || modelSave.upserted.length > 0) {
        const session = new Session({
          action: 'Actualización de registro',
          collectionName,
          email: user.email,
          user
        })
        generalFunctions.createSesion(session)
        responseService.resultResponse({
          type: 'save',
          status: 201,
          res
        })
      } else {
        const error = {
          message: 'Ha ocurrido un error al guardar los cambios en base de datos'
        }
        responseService.resultResponse({
          type: 'error',
          status: 400,
          data: error,
          res
        })
      }
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}
/**
 * UPDATE ONE
 * @param {*} Model
 * @param {*} collectionName
 * @param {*} params
 * @param {*} data
 * @param {*} req
 * @param {*} res
 * @description :: Update one document from DB
 */

generalFunctions.updateOneSession = (
  Model,
  collectionName,
  params,
  data,
  req,
  res,
  message = null
) => {
  const user = req.user
  Model.updateOne(params, data)
    .then((modelSave) => {
      if (modelSave.nModified > 0) {
        const session = new Session({
          action: message || 'Actualización de registro',
          collectionName,
          email: user.email,
          user
        })
        generalFunctions.createSesion(session)
        responseService.resultResponse({
          type: 'update',
          status: 200,
          res
        })
      } else {
        const error = {
          message: 'Ha ocurrido un error al guardar los cambios en base de datos'
        }
        responseService.resultResponse({
          type: 'error',
          status: 400,
          data: error,
          res
        })
      }
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

/**
 * DELETE PHYSICAL
 * @param {*} Model
 * @param {*} params
 * @param {*} res
 * @description :: PHYSICALLY DELETE THE DATABASE RECORDS
 */
generalFunctions.deletePhysical = (Model, params, res = null) => {
  Model.deleteOne(params)
    .exec()
    .then((deleteModel) => {
      if (res) {
        if (deleteModel.deletedCount === 1) {
          responseService.resultResponse({
            type: 'update',
            status: 200,
            res
          })
        } else {
          responseService.resultResponse({
            type: 'error',
            status: 400,
            data: {
              message: 'Ha ocurrido un error al eliminar este registro de Base de Datos'
            },
            res
          })
        }
      }
    })
    .catch((err) => {
      if (res) {
        responseService.resultResponse({
          type: 'error',
          status: 500,
          data: err,
          res
        })
      }
    })
}

/**
 * DELETE PHYSICAL
 * @param {*} Model
 * @param {*} collectionName
 * @param {*} params
 * @param {*} req
 * @param {*} res
 * @description :: PHYSICALLY DELETE THE DATABASE RECORDS
 */
generalFunctions.deletePhysicalSession = (
  Model,
  collectionName,
  params,
  req,
  res,
  returnMessage = true
) => {
  const user = req.user
  Model.deleteOne(params)
    .exec()
    .then((deleteModel) => {
      if (deleteModel.deletedCount === 1) {
        const session = new Session({
          action: 'Eliminación de registro',
          collectionName,
          email: user.email,
          user
        })
        generalFunctions.createSesion(session)
        if (returnMessage) {
          responseService.resultResponse({
            type: 'update',
            status: 200,
            res
          })
        }
      } else {
        const err = {
          message: 'Ha ocurrido un error al eliminar este registro de Base de Datos'
        }
        if (returnMessage) {
          responseService.resultResponse({
            type: 'error',
            status: 400,
            data: err,
            res
          })
        }
      }
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

/**
 * DELETE MANY PHYSICAL
 *
 *
 * @param {*} Model
 * @param {*} collectionName
 * @param {*} params
 * @param {*} req
 * @param {*} res
 * @description :: PHYSICALLY DELETE  THE DATABASE RECORDS
 */
generalFunctions.deleteManyPhysical = function (Model, collectionName, params, req, res) {
  const user = req.user
  Model.deleteMany(params)
    .exec()
    .then((deleteModel) => {
      if (deleteModel.deletedCount > 0) {
        const session = new Session({
          action: `Eliminación de (${deleteModel.deletedCount}) registros`,
          collectionName,
          email: user.email,
          user
        })
        generalFunctions.createSesion(session)
        responseService.resultResponse({
          type: 'update',
          status: 200,
          res
        })
      } else {
        const err = {
          message: 'Ha ocurrido un error al eliminar los registros de Base de Datos'
        }
        responseService.resultResponse({
          type: 'error',
          status: 400,
          data: err,
          res
        })
      }
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}
/**
 * GET ALL POPULATE
 *
 * @param {*} Model
 * @param {*} populate
 * @param {*} res
 * @param {*} filter
 * @description :: List of all documents of a models from DB
 */
generalFunctions.getAllPopulate = (Model, populate, res, filter = {}) => {
  Model.find(filter)
    .populate(populate)
    .exec()
    .then((modelFind) => {
      responseService.resultResponse({
        type: 'list',
        status: 200,
        data: modelFind,
        res
      })
    })
    .catch(err =>
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    )
}

/**
 * GET ALL PARAMS
 * @param :: getAllObject = {model, params, res, populate,sort}
 * @description :: List of all documents of a models from DB
 */

generalFunctions.getAllParams = (getAllObject) => {
  const {
    model,
    params,
    res,
    populate,
    sort
  } = getAllObject
  model.find(params)
    .sort(sort || '')
    .populate(populate || '')
    .exec()
    .then((modelFind) => {
      responseService.resultResponse({
        type: 'list',
        status: 200,
        data: modelFind,
        res
      })
    })
    .catch(err =>
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    )
}

/**
 * GET ONE BY PARAMS
 *
 * @param {*} Model
 * @param {*} params
 * @param {*} populate
 * @param {*} res
 * @description :: Get One document of a model from DB
 */
generalFunctions.getOnePopulate = (Model, params, populate, res) => {
  Model.findOne(params)
    .populate(populate)
    .exec()
    .then((modelFind) => {
      responseService.resultResponse({
        type: 'list',
        status: 200,
        data: modelFind,
        res
      })
    })
    .catch(err =>
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    )
}

/**
 * GET ALL PAGINATE
 *
 * @param {*} Model
 * @param {*} filter
 * @param {*} req
 * @param {*} res
 * @param {*} populate
 * @description :: GET ALL ITEMS FROM A MODEL
 */
generalFunctions.getAllPaginate = (Model, filter, req, res, populate = '') => {
  const body = req.body
  const {
    sortDirection,
    sortBy
  } = body
  const currentPage = Number(body.currentPage)
  const limit = Number(body.perPage) || 50
  const skip = (currentPage - 1) * limit

  Model.countDocuments(filter)
    .then((filterRecord) => {
      const totalRows = filterRecord
      Model.find(filter)
        .sort({
          [sortBy]: sortDirection
        })
        .skip(skip)
        .limit(limit)
        .populate(populate)
        .lean()
        .exec()
        .then((results) => {
          res.json({
            item: results,
            totalRows
          })
        })
        .catch((err) => {
          responseService.resultResponse({
            type: 'error',
            status: 500,
            data: err,
            res
          })
        })
    })
    .catch(err =>
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    )
}
/**
 * sleep
 * @description :: stop execution for a couple of minutes, so the report doesn't stagnate
 */
generalFunctions.sleep = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms))
}

/**
 * getRequest
 *
 * @description :: GET DATA FROM ANOTHER API
 * @param:: options :: OPTIONS TO CONSULT ANY REPORT OR DATA
 * @returns :: DATA (REPORTS)
 */

generalFunctions.getRequest = (options) => {
  return new Promise((resolve, reject) => {
    axios(options)
      .then((result) => {
        resolve(result.data)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

/**
 * saveFile: save file in a folder on the server
 * @param {*} urlBase :: folder URL
 * @param {*} file :: file to save
 * @param {*} fileName :: name of file
 * @param {*} res
 * @param {*} response
 */
generalFunctions.saveFile = (saveFileObject) => {
  const {
    urlBase,
    file,
    fileName,
    res,
    response
  } = {
    ...saveFileObject
  }

  if (!fs.existsSync(urlBase)) {
    fs.mkdirSync(urlBase)
  }
  file
    .mv(fileName)
    .then((uploadFile) => {
      if (response) {
        responseService.resultResponse({
          type: 'save',
          status: 201,
          res
        })
      }
    })
    .catch((err) => {
      err.message = 'Ha ocurrido un error almacenando la imagen'
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

generalFunctions.createSesion = (session) => {
  session
    .save()
    .then(() => console.log('Crea una accion'))
    .catch(err => console.log(err))
}
module.exports = generalFunctions
