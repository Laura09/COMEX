const {
  google
} = require('googleapis')
const credentials = require('../../token.json')
const scopes = ['https://www.googleapis.com/auth/drive']
const driveService = {}

/**
 * driveService
 *
 * @description :: GOOGLE DRIVE AUTH
 */
driveService.driveService = () => {
  const auth = new google.auth.JWT(
    credentials.client_email, null,
    credentials.private_key, scopes
  )
  return google.drive({
    version: 'v3',
    auth
  })
}

/**
 * createFile
 * @param :: createFileObject = {folderId,fileObject,bufferStream}
 * folderId :: folder id in drive
 * fileObject :: document
 * bufferStream :: file transformed into bufferedStream
 * @description :: CREATE FILE IN DRIVE
 */
driveService.createFile = (createFileObject) => {
  const {
    folderId,
    fileObject,
    bufferStream
  } = createFileObject
  return new Promise((resolve, reject) => {
    const fileMetadata = {
      name: fileObject.name,
      parents: [folderId]
    }
    const media = {
      mimeType: fileObject.mimetype,
      body: bufferStream
    }
    driveService.driveService().files.create({
      resource: fileMetadata,
      media,
      fields: 'id, name, webContentLink, webViewLink, iconLink, thumbnailLink'
    }, (err, file) => {
      if (err) {
        // Handle error
        reject(err)
      } else {
        resolve(file.data)
      }
    })
  })
}

driveService.searchElement = (searchObject) => {
  const { query, fields } = searchObject
  return new Promise((resolve, reject) => {
    driveService.driveService().files.list({
      q: query || '',
      ...fields ? { fields } : ''
    }, (err, resList) => {
      if (err) {
        reject(err)
      } else {
        resolve(resList.data.files)
      }
    })
  })
}

driveService.delete = (fileId) => {
  return new Promise((resolve, reject) => {
    driveService.driveService().files.delete({
      fileId
    }, (err, deleted) => {
      if (err) {
        reject(err)
      } else {
        resolve(deleted)
      }
    })
  })
}

driveService.downloadFile = (fileId) => {
  return new Promise((resolve, reject) => {
    driveService.driveService().files.get({
      fileId, alt: 'media'
    }, { responseType: 'stream' }, (err, { data }) => {
      if (err) {
        reject(err)
      } else {
        const buf = []
        data.on('data', e => buf.push(e))
        data.on('end', () => {
          const buffer = Buffer.concat(buf)
          resolve(buffer)
        })
      }
    })
  })
}

module.exports = driveService
