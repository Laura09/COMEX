const express = require('express')
const router = express.Router()
const chatController = require('../controllers/chatController')
const middleware = require('../middlewares/authMiddleware')

router.post('/chat', middleware.verifyToken, chatController.getAll)
router.post('/chat/create', middleware.verifyToken, chatController.create)
router.put('/chat/update/:_id', middleware.verifyToken, chatController.update)
router.delete('/chat/delete/:_id', middleware.verifyToken, chatController.delete)
module.exports = router
