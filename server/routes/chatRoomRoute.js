const express = require('express')
const router = express.Router()
const chatRoomController = require('../controllers/chatRoomController')
const middleware = require('../middlewares/authMiddleware')

router.get('/chatRoom', middleware.verifyToken, chatRoomController.getAll)
router.get('/chatRoom/:name', middleware.verifyToken, chatRoomController.getOne)
router.post('/chatRoom/create', middleware.verifyToken, chatRoomController.create)
router.put('/chatRoom/update/:_id', middleware.verifyToken, chatRoomController.update)
router.delete('/chatRoom/delete/:_id', middleware.verifyToken, chatRoomController.delete)
module.exports = router
