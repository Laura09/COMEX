const express = require('express')
const router = express.Router()
const authController = require('../controllers/authController')
const middleware = require('../middlewares/authMiddleware')

router.post('/auth/login', authController.login)
router.get('/auth/me', middleware.verifyToken, authController.userDetail)
module.exports = router
