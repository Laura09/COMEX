const express = require('express')
const router = express.Router()
const sessionController = require('../controllers/sessionController')
const middleware = require('../middlewares/authMiddleware')

router.post('/session', middleware.verifyToken, sessionController.getAll)
router.delete(
  '/session/delete/:_id',
  middleware.verifyToken,
  sessionController.delete
)
module.exports = router
