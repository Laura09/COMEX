const express = require('express')
const router = express.Router()
const exportController = require('../controllers/exportController')
const middleware = require('../middlewares/authMiddleware')

router.post('/export', middleware.verifyToken, exportController.getAll)
router.post('/export/create', middleware.verifyToken, exportController.create)
router.get('/export/getOne/:_id', middleware.verifyToken, exportController.getOne)
router.put('/export/update/:_id', middleware.verifyToken, exportController.update)
router.delete('/export/delete/:_id', middleware.verifyToken, exportController.delete)

module.exports = router
