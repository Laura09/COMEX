const http = require('http')
const express = require('express')
const app = express()
const server = http.createServer(app)
const io = require('socket.io')(server, {
  cors: `http://${process.env.AXIOS_URL}:${process.env.AXIOS_PORT}`
})
const compression = require('compression')
const fileUpload = require('express-fileupload') // fileUpload
const bodyParser = require('body-parser')
const db = require('../config/dbConfig')
db.connection()

io.on('connection', (socket) => {
  console.log('Client connected ON SERVER')
  socket.on('disconnect', () => {
    console.log('Disconnected: ' + socket)
  })

  socket.on('join', function (data) {
    console.log(data)
    socket.join(data.chatRoom) // We are using room of socket io
  })
  socket.on('save-message', (data) => {
    console.log('envia mensaje')
    io.sockets.to(data.chatRoom).emit('new-message', { message: data })
  })
  socket.on('leave', function (data) {
    socket.leave(data.chatRoom) // We are using room of socket io
  })
  socket.on('private-message', ({ message, to }) => {
    to.forEach((email) => {
      io.sockets.to(email).emit('private-message', {
        message
      })
    })
  })
})
io.on('connect_error', (err) => {
  console.log(`connect_error due to ${err},`)
})
const referenceRouter = require('./referenceRoute')
const privilegeRouter = require('./privilegeRoute')
const templateRouter = require('./templateRoute')
const chatRoomRouter = require('./chatRoomRoute')
const sessionRouter = require('./sessionRoute')
const exportRouter = require('./exportRoute')
const emailRouter = require('./emailRoute')
const chatRouter = require('./chatRoute')
const roleRouter = require('./roleRoute')
const authRouter = require('./authRoute')
const fileRouter = require('./fileRoute')
const userRouter = require('./userRoute')
app.use(fileUpload())
app.use(compression())
app.use(express.json({ limit: '10000mb' }))
app.use(express.urlencoded({ extended: true, limit: '10000mb' }))
app.use(bodyParser.json())
app.use(referenceRouter)
app.use(privilegeRouter)
app.use(chatRoomRouter)
app.use(templateRouter)
app.use(sessionRouter)
app.use(exportRouter)
app.use(emailRouter)
app.use(chatRouter)
app.use(fileRouter)
app.use(authRouter)
app.use(roleRouter)
app.use(userRouter)

// Export the server middleware
module.exports = {
  path: '/api',
  handler: app
}
