const express = require('express')
const router = express.Router()
const roleController = require('../controllers/roleController')
const middleware = require('../middlewares/authMiddleware')

router.get('/role', middleware.verifyToken, roleController.getAll)
router.post('/role/create', middleware.verifyToken, roleController.create)
router.put('/role/update/:_id', middleware.verifyToken, roleController.update)
router.delete('/role/delete/:_id', middleware.verifyToken, roleController.delete)
module.exports = router
