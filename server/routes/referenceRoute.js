const express = require('express')
const router = express.Router()
const referenceController = require('../controllers/referenceController')
const middleware = require('../middlewares/authMiddleware')

router.post('/reference/list/:type', middleware.verifyToken, referenceController.getAll)
router.post('/reference/create', middleware.verifyToken, referenceController.create)
router.put('/reference/update/:_id', middleware.verifyToken, referenceController.update)
router.put('/reference/upsertBulk', middleware.verifyToken, referenceController.upsertBulk)
router.delete('/reference/delete/:_id', middleware.verifyToken, referenceController.delete)
router.delete('/reference/deleteMany', middleware.verifyToken, referenceController.deleteMany)

module.exports = router
