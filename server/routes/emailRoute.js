const express = require('express')
const router = express.Router()
const emailController = require('../controllers/emailController')
const middleware = require('../middlewares/authMiddleware')

router.get('/email', middleware.verifyToken, emailController.getAll)
router.post('/email/create', middleware.verifyToken, emailController.create)
router.put('/email/update/:_id', middleware.verifyToken, emailController.update)
router.delete('/email/delete/:_id', middleware.verifyToken, emailController.delete)
module.exports = router
