const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')
const middleware = require('../middlewares/authMiddleware')

router.post('/user', middleware.verifyToken, userController.getAll)
router.post('/user/create', middleware.verifyToken, userController.create)
router.put('/user/update/:_id', middleware.verifyToken, userController.update)
router.delete('/user/delete/:_id', middleware.verifyToken, userController.delete)
module.exports = router
