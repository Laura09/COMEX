const express = require('express')
const router = express.Router()
const privilegeController = require('../controllers/privilegeController')
const middleware = require('../middlewares/authMiddleware')

router.get('/privilege', middleware.verifyToken, privilegeController.getAll)
router.get('/privilege/getParents', middleware.verifyToken, privilegeController.getParents)
router.get('/privilege/getByRole/:role', middleware.verifyToken, privilegeController.getByRole)
router.post('/privilege/create', middleware.verifyToken, privilegeController.create)
router.put('/privilege/updateRoles', middleware.verifyToken, privilegeController.updateRoles)
router.put('/privilege/update/:_id', middleware.verifyToken, privilegeController.update)
router.delete('/privilege/delete/:_id', middleware.verifyToken, privilegeController.delete)
module.exports = router
