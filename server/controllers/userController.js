const User = require('../models/userModel')
const dbService = require('../services/dbService')

const userController = {}

/**
 * CREATE
 *
 * @description :: CREATE ITEM IN DB
 */
userController.create = (req, res) => {
  const data = req.body
  const { password } = data
  const user = new User({
    ...data
  })
  user.password = user.generateHash(password)
  dbService.createOneSession(User, 'User', user, req, res)
}

/**
 * GET ALL
 *
 * @description :: list of all items in db
 */

userController.getAll = (req, res) => {
  const { search } = req.body
  const searchName =
    search && search !== '' ? new RegExp(search.trim(), 'i') : ''
  const filter = {
    ...(searchName !== ''
      ? {
          email: searchName
        }
      : '')
  }

  dbService.getAllPaginate(User, filter, req, res, [
    { path: 'brand', select: 'reference code' },
    { path: 'country', select: 'reference code' },
    { path: 'role', select: 'role' }
  ])
}

/**
 * UPDATE ITEM
 *
 * @description :: UPDATE ONE ITEM IN DB
 */

userController.update = (req, res) => {
  const { _id } = req.params
  const body = { ...req.body }
  if (body.changePassword) {
    const user = new User({})
    body.password = user.generateHash(body.password)
  }

  dbService.updateOneSession(
    User,
    'User',
    {
      _id
    },
    body,
    req,
    res
  )
}

/**
 * DELETE ITEM
 *
 * @description :: DELETE ONE ITEM IN DB
 */

userController.delete = (req, res) => {
  const { _id } = req.params
  dbService.deletePhysicalSession(
    User,
    'User',
    {
      _id
    },
    req,
    res
  )
}

/**
 * DELETE SESSION
 *
 * @description :: DELETE USER SESSION IN DB
 */

userController.deleteMany = (req, res) => {
  const { data } = req.body
  dbService.deleteManyPhysical(
    User,
    'User',
    {
      _id: { $in: JSON.parse(data) }
    },
    req,
    res
  )
}
module.exports = userController
