const fs = require('fs')
const path = require('path')
const async = require('async')
const Reference = require('../models/referenceModel')
const FileModel = require('../models/fileModel')
const dbService = require('../services/dbService')
const responseService = require('../services/responseService')

const referenceController = {}

/**
 * CREATE
 *
 * @description :: CREATE ITEM IN DB
 */
referenceController.create = (req, res) => {
  const data = req.body
  const { file } = {
    ...req.files
  }

  if (file) {
    const createReference = new Reference({
      ...data
    })
    const fileName = file.name.split('.')
    const fileExtension = fileName[fileName.length - 1]
    createReference.fileName = `${createReference._id}.${fileExtension}`
    const pathFile = path.resolve(__dirname, '../../static/image')
    const fileData = {
      urlBase: pathFile,
      fileName: `${pathFile}/${createReference.fileName}`,
      file,
      res
    }
    dbService.createWithImage(
      Reference,
      'Reference',
      createReference,
      fileData,
      req,
      res
    )
  } else {
    dbService.upsertOneSession(
      Reference,
      'Reference',
      {
        reference: new RegExp(`^${data.reference}$`, 'i'),
        type: new RegExp(`^${data.type}$`, 'i')
      },
      data,
      req,
      res
    )
  }
}

/**
 * GET ALL
 *
 * @description :: list of all items in db
 */

referenceController.getAll = (req, res) => {
  const { search, fileName, userBrands, userCountries } = req.body
  const { type } = req.params
  const searchName =
    search && search !== '' ? new RegExp(search.trim(), 'i') : ''
  const filter = {
    type,
    ...(fileName ? { fileName: { $ne: null } } : ''),
    ...(searchName !== ''
      ? {
          $or: [
            {
              reference: searchName
            },
            {
              code: searchName
            }
          ]
        }
      : ''),
    ...(userBrands && userBrands.length > 0
      ? { _id: { $in: userBrands } }
      : ''),
    ...(userCountries && userCountries.length > 0
      ? { _id: { $in: userCountries } }
      : '')
  }

  dbService.getAllPaginate(Reference, filter, req, res)
}

/**
 * UPDATE ITEM
 *
 * @description :: UPDATE ONE ITEM IN DB
 */

referenceController.update = (req, res) => {
  const { _id } = req.params
  const { file } = {
    ...req.files
  }
  const body = req.body
  if (file) {
    const pathFile = path.resolve(__dirname, '../../static/image')
    const fileName = file.name.split('.')
    const fileExtension = fileName[fileName.length - 1]
    const fileUpdate = `${_id}.${fileExtension}`
    const fileData = {
      urlBase: pathFile,
      fileName: `${pathFile}/${req.body.fileName || fileUpdate}`,
      file,
      res
    }
    body.fileName = req.body.fileName || fileUpdate
    dbService.updateWithImage(
      Reference,
      'Reference',
      {
        _id
      },
      body,
      fileData,
      req,
      res
    )
  } else {
    dbService.updateOneSession(
      Reference,
      'Reference',
      {
        _id
      },
      body,
      req,
      res
    )
  }
}

/**
 * DELETE ITEM
 *
 * @description :: DELETE ONE ITEM IN DB
 */

/**
 * UPSERT ITEMS IN BULK
 *
 * @description :: UPSERT ITEMS BULK
 */

referenceController.upsertBulk = (req, res) => {
  const { type } = {
    ...req.query
  }
  const { data } = req.body
  const arrayObjects = JSON.parse(data)
  const bulkOps = []
  const fileBulkOps = [] // UPDATE FILE MODEL
  async.each(
    arrayObjects,
    (item, callback) => {
      const { reference, code } = {
        ...item
      }
      if (reference) {
        const upsertDoc = {
          updateOne: {
            filter: {
              reference,
              type
            },
            update: {
              $setOnInsert: {
                createdAt: new Date()
              },
              $set: {
                ...item,
                updatedAt: new Date(),
                type
              }
            },
            upsert: true
          }
        }
        bulkOps.push(upsertDoc)
        if (type === 'reference') {
          const fileUpdateObject = [
            {
              updateMany: {
                filter: {
                  'items.reference': reference,
                  type: 'Fact'
                },
                update: {
                  $set: {
                    'items.$.sku': code,
                    'items.$.incorrect': false
                  }
                },
                upsert: false
              }
            },
            {
              updateMany: {
                filter: {
                  'items.reference': reference,
                  'items.sku': code,
                  'items.incorrect': false,
                  type: 'PL'
                },
                update: {
                  $set: {
                    'items.$.wrongReference': false
                  }
                },
                upsert: false
              }
            }
          ]
          fileBulkOps.push(...fileUpdateObject)
        }
      }
      callback()
    },
    (err) => {
      // if any of the file processing produced an error, err would equal that error
      if (err) {
        // One of the iterations produced an error.
        // All processing will now stop.
        responseService.resultResponse({
          type: 'error',
          status: 500,
          data: {
            message: 'Ha ocurrido un error al almacenar la información'
          },
          res
        })
      } else if (bulkOps.length > 0) {
        Reference.collection
          .bulkWrite(bulkOps)
          .then(async () => {
            if (fileBulkOps.length > 0) {
              await FileModel.collection.bulkWrite(fileBulkOps)
            }
            responseService.resultResponse({
              type: 'update',
              status: 201,
              res
            })
          })
          .catch((err) => {
            responseService.resultResponse({
              type: 'error',
              status: 500,
              data: err,
              res
            })
          })
      } else {
        responseService.resultResponse({
          type: 'custom',
          status: 400,
          data: {
            message: 'El archivo ingresado, no posee datos correctos',
            type: 'warning'
          },
          res
        })
      }
    }
  )
}
referenceController.delete = (req, res) => {
  const { _id } = req.params
  const body = req.body
  if (body.fileName && body.fileName !== '') {
    const pathFile = path.resolve(
      __dirname,
      `../../static/image/${body.fileName}`
    )
    if (fs.existsSync(pathFile)) {
      fs.unlinkSync(pathFile) // Delete the file
    }
  }
  dbService.deletePhysicalSession(
    Reference,
    'Reference',
    {
      _id
    },
    req,
    res
  )
}

/**
 * DELETE SESSION
 *
 * @description :: DELETE USER SESSION IN DB
 */

referenceController.deleteMany = (req, res) => {
  const { data } = req.body
  dbService.deleteManyPhysical(
    Reference,
    'Reference',
    {
      _id: {
        $in: JSON.parse(data)
      }
    },
    req,
    res
  )
}
module.exports = referenceController
