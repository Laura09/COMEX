const fs = require('fs')
const path = require('path')
const Excel = require('exceljs')
const Template = require('../models/templateModel')
const Reference = require('../models/referenceModel')
const Session = require('../models/sessionModel')
const dbService = require('../services/dbService')
const responseService = require('../services/responseService')
const templateController = {}

/**
 * CREATE
 *
 * @description :: CREATE ITEM IN DB
 */
templateController.create = (req, res) => {
  const data = req.body
  const user = req.user
  const { header, columns, exampleRow, title } = data

  const createData = new Template({
    ...data
  })
  const fileName = `/files/${createData._id}.xlsx`
  createData.fileName = fileName
  Template.create(createData)
    .then(() => {
      const workbook = new Excel.Workbook() // excel object
      createExcelTemplate({
        action: 'Creación de nuevo registro',
        workbook,
        fileName,
        user,
        header,
        columns,
        exampleRow,
        title,
        res
      })
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

/**
 * GET ALL
 *
 * @description :: list of all items in db
 */

templateController.getAll = (req, res) => {
  const { search } = req.body
  const searchName =
    search && search !== '' ? new RegExp(search.trim(), 'i') : ''
  const filter = {
    ...(searchName !== ''
      ? {
          title: searchName
        }
      : '')
  }

  dbService.getAllPaginate(Template, filter, req, res)
}

templateController.getOne = (req, res) => {
  const { title } = req.params
  dbService.getOnePopulate(
    Template,
    {
      title
    },
    '',
    res
  )
}
/**
 * UPDATE ITEM
 *
 * @description :: UPDATE ONE ITEM IN DB
 */

templateController.update = (req, res) => {
  const { _id } = req.params
  const data = req.body
  const user = req.user
  const { header, columns, exampleRow, title } = data
  Template.updateOne(
    {
      _id
    },
    data
  )
    .then((modelSave) => {
      if (modelSave.nModified > 0) {
        const pathFile = path.resolve(
          __dirname,
          `../../static/${data.fileName}`
        )
        const workbook = new Excel.Workbook()
        workbook.xlsx
          .readFile(pathFile)
          .then(() => {
            const worksheet = workbook.getWorksheet(1)
            // Remove the worksheet using worksheet id
            workbook.removeWorksheet(worksheet.id)
            createExcelTemplate({
              action: 'Actualización de registro',
              fileName: data.fileName,
              workbook,
              user,
              header,
              columns,
              exampleRow,
              title,
              res
            })
          })
          .catch(err =>
            responseService.resultResponse({
              type: 'error',
              status: 500,
              data: err,
              res
            })
          )
      } else {
        const error = {
          message:
            'Ha ocurrido un error al guardar los cambios en base de datos'
        }
        responseService.resultResponse({
          type: 'error',
          status: 400,
          data: error,
          res
        })
      }
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

templateController.createBrandTemplate = async (req, res) => {
  const { brand, fileName } = req.body
  const errorLabel = {
    message:
      'Plantilla no disponible'
  }
  try {
    const brandFind = await Reference.findOne({
      type: 'brand',
      _id: brand
    }).exec()

    if (brandFind) {
      const imageFile = path.resolve(
        __dirname,
        `../../static/image/${brandFind.fileName}`
      )

      if (fs.existsSync(imageFile)) {
        const templateFind = await Template.findOne({
          title: fileName
        }).exec()
        if (templateFind) {
          const pathTemplateFile = path.resolve(
            __dirname,
            `../../static/${templateFind.fileName}`
          )

          if (fs.existsSync(pathTemplateFile)) {
            const workbook = new Excel.Workbook()
            workbook.xlsx
              .readFile(pathTemplateFile)
              .then(async () => {
                const image = brandFind.fileName.split('.')
                const worksheet = workbook.getWorksheet(1)
                const excelImage = workbook.addImage({
                  buffer: fs.readFileSync(imageFile),
                  extension: image[1]
                })
                worksheet.addImage(excelImage, {
                  tl: {
                    col: 1,
                    row: 2
                  },
                  br: {
                    col: 3,
                    row: 7
                  },
                  editAs: 'undefined'
                })
                const excelBuffer = await workbook.xlsx.writeBuffer()
                const pathNewExcelFile = path.resolve(
                  __dirname,
                  '../../static/brandExcel.xlsx'
                )
                const document = fs.createWriteStream(pathNewExcelFile)
                document.write(excelBuffer)
                req.pipe(document)
                document.on('finish', () => {
                  res.download(pathNewExcelFile, (err) => {
                    if (err) {
                      responseService.resultResponse({
                        type: 'error',
                        status: 500,
                        data: err,
                        res
                      })
                    } else if (fs.existsSync(pathNewExcelFile)) {
                      fs.unlinkSync(pathNewExcelFile) // Delete the file
                    }
                  })
                })
              })
              .catch(err =>
                responseService.resultResponse({
                  type: 'error',
                  status: 500,
                  data: err,
                  res
                })
              )
          } else {
            responseService.resultResponse({
              type: 'error',
              status: 400,
              data: errorLabel,
              res
            })
          }
        } else {
          responseService.resultResponse({
            type: 'error',
            status: 400,
            data: errorLabel,
            res
          })
        }
      } else {
        responseService.resultResponse({
          type: 'error',
          status: 400,
          data: errorLabel,
          res
        })
      }
    } else {
      responseService.resultResponse({
        type: 'error',
        status: 400,
        data: errorLabel,
        res
      })
    }
  } catch (err) {
    responseService.resultResponse({
      type: 'error',
      status: 500,
      data: err,
      res
    })
  }
}

/**
 * DELETE ITEM
 *
 * @description :: DELETE ONE ITEM IN DB
 */

templateController.delete = (req, res) => {
  const { _id } = req.params
  const fileName = `/files/${_id}.xlsx`
  const pathFile = path.resolve(__dirname, `../../static/${fileName}`) // Path of file
  if (fs.existsSync(pathFile)) {
    fs.unlinkSync(pathFile) // Delete the file
  }
  dbService.deletePhysicalSession(
    Template,
    'Template',
    {
      _id
    },
    req,
    res
  )
}
/**
 *
 * @param {*} excelTemplateObject :: { title, fileName, action, header, columns, exampleRow, user, res }
 */
function createExcelTemplate (excelTemplateObject) {
  const {
    title,
    fileName,
    action,
    header,
    columns,
    exampleRow,
    user,
    workbook,
    res
  } = excelTemplateObject
  const pathFile = path.resolve(__dirname, `../../static/${fileName}`) // Path of file
  const worksheet = workbook.addWorksheet(title, {
    properties: {
      defaultColWidth: '25'
    }
  })
  if (header && header.length > 0) {
    let cellIndex = 3
    header.forEach((value) => {
      worksheet.getCell(`E${cellIndex}`).value = value.toUpperCase()
      worksheet.getCell(`E${cellIndex}`).font = {
        size: 12,
        bold: true
      }
      cellIndex += 1
    })
    worksheet.mergeCells('C9', 'E9')
    worksheet.getCell('C9').value = title.toUpperCase()
    worksheet.getCell('C9').font = {
      size: 14,
      bold: true
    }
    worksheet.getCell('C9').alignment = {
      horizontal: 'center'
    }
  }
  const rowIndex = header && header.length > 0 ? 11 : 1
  worksheet.getRow(rowIndex).values = columns
  worksheet.getRow(rowIndex).eachCell((_cell, colNumber) => {
    worksheet.getRow(rowIndex).getCell(colNumber).style = {
      fill: {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {
          argb: '000000'
        },
        bgColor: {
          argb: '000000'
        }
      },
      font: {
        color: {
          argb: 'FFFFFFFF',
          bold: true
        }
      }
    }
  })
  if (exampleRow) {
    worksheet.addRow(header && header.length > 0 ? 12 : 2).values = exampleRow
  }
  workbook.xlsx
    .writeFile(pathFile)
    .then(() => {
      const session = new Session({
        action,
        collectionName: 'Template',
        email: user.email,
        user
      })
      dbService.createSesion(session)
      responseService.resultResponse({
        type: 'save',
        status: 201,
        res
      })
    })
    .catch(err =>
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    )
}
module.exports = templateController
