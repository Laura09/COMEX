const Email = require('../models/emailModel')
const dbService = require('../services/dbService')
const roleController = {}

/**
 * CREATE
 *
 * @description :: CREATE ITEM IN DB
 */
roleController.create = (req, res) => {
  const data = req.body
  dbService.createOneSession(Email, 'Email', data, req, res)
}

/**
 * GET ALL
 *
 * @description :: list of all items in db
 */

roleController.getAll = (req, res) => {
  dbService.getAllParams({ model: Email, params: {}, res })
}

/**
 * UPDATE ITEM
 *
 * @description :: UPDATE ONE ITEM IN DB
 */

roleController.update = (req, res) => {
  const { _id } = req.params
  dbService.updateOneSession(
    Email,
    'Email',
    {
      _id
    },
    req.body,
    req,
    res
  )
}

/**
 * DELETE ITEM
 *
 * @description :: DELETE ONE ITEM IN DB
 */

roleController.delete = (req, res) => {
  const { _id } = req.params
  dbService.deletePhysicalSession(
    Email,
    'Email',
    {
      _id
    },
    req,
    res
  )
}

module.exports = roleController
