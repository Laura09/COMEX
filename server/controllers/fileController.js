const stream = require('stream')
const fs = require('fs')
const path = require('path')
const async = require('async')
const mongoose = require('mongoose')
const Excel = require('exceljs')
const nodemailer = require('nodemailer')
const File = require('../models/fileModel')
const exportModel = require('../models/exportModel')
const dbService = require('../services/dbService')
const driveService = require('../services/driveService')
const extrenalService = require('../services/externalService')
const responseService = require('../services/responseService')
const templateModel = require('../models/templateModel')
const referenceModel = require('../models/referenceModel')
const emailModel = require('../models/emailModel')
const Session = require('../models/sessionModel')
const transport = require('../config/transport')

const driveFolder = process.env.DRIVEFOLDER
const pedidosEmail = process.env.PEDIDOS_EMAIL
const pedidosPass = process.env.PEDIDOS_PASS

const fileController = {}
/**
 * CREATE
 *
 * @description :: CREATE ITEM IN DB
 */
fileController.create = async (req, res) => {
  const user = {
    ...req.user
  }
  try {
    let data = { ...req.body, user }
    const { fileData, invoiceNumber, exportID, packingListType, comexFile } = {
      ...data
    }
    let { country, brand } = { ...data }
    country = JSON.parse(country)
    brand = JSON.parse(brand)
    const { file } = {
      ...req.files
    }
    const fileFind = await File.findOne({
      type: data.type,
      invoiceNumber,
      exportID
    }).exec()

    if (!fileFind) {
      const fileName = file.name.split('.')
      const fileExtension = fileName[fileName.length - 1]
      const bufferStream = new stream.PassThrough()
      bufferStream.end(file.data)
      file.name = invoiceNumber
      const driveFile = await driveService.createFile({
        folderId: driveFolder,
        fileObject: file,
        bufferStream
      })
      if (driveFile) {
        const { id, name } = driveFile
        data = {
          ...data,
          fileId: id,
          fileName: name,
          comexFile: !!comexFile,
          fileExtension,
          country,
          brand
        }

        const sendFileData = JSON.parse(fileData)
        const createItems = {
          PL: () =>
            createPackingList({
              country: country.code || 'País Eliminado',
              brand: brand.reference || 'Marca Eliminada',
              sendFileData,
              packingListType,
              invoiceNumber
            }),
          Fact: () => createInvoiceItems(sendFileData),
          FactPDF: () => sendFileData,
          CO: () => [],
          Otros: () => []
        }
        const items = await createItems[data.type]()
        data.items = items
        const createdFile = await File.create(data)
        if (createdFile) {
          const fileModelMessage = {
            PL: `Creación de Lista de Empaque: ${data.fileName}`,
            CO: `Creación de Certificado de Origen:  ${data.fileName}`,
            Fact: `Creación de Factura: ${data.fileName}`,
            FactPDF: `Creación de Factura PDF: ${data.fileName}`,
            Otros: `Creación de Otros documentos: ${data.fileName}`
          }

          const session = new Session({
            email: user.email,
            action: fileModelMessage[data.type],
            collectionName: 'File Model',
            user
          })
          dbService.createSesion(session)
          responseService.resultResponse({
            type: 'save',
            status: 201,
            res
          })
          if (data.type === 'PL') {
            sendCodesNotFound({
              packingList: items,
              user
            })
          }
        }
      }
    } else {
      responseService.resultResponse({
        type: 'custom',
        status: 409,
        data: {
          message: 'Documento existente, ingrese un nuevo documento',
          type: 'warning'
        },
        res
      })
    }
  } catch (err) {
    responseService.resultResponse({
      type: 'error',
      status: 500,
      data: err,
      res
    })
    const session = new Session({
      email: user.email,
      action: `Error al importar un archivo ${err.message}`,
      error: true,
      collectionName: 'File Model',
      user
    })
    dbService.createSesion(session)
  }
}

/**
 * UPDATE
 *
 * @description :: UPDATE ITEM IN DB
 */
fileController.update = async (req, res) => {
  try {
    const user = {
      ...req.user
    }
    const { _id } = req.params
    let data = req.body
    const { fileData, invoiceNumber, packingListType, comexFile } = data
    let { country, brand } = { ...data }
    if (country) {
      country = JSON.parse(country)
    }
    if (brand) {
      brand = JSON.parse(brand)
    }
    const { file } = {
      ...req.files
    }
    if (file) {
      const deleteFile = await driveService.delete(data.fileId)
      if (deleteFile) {
        const imageName = file.name.split('.')
        const fileExtension = imageName[imageName.length - 1]
        const bufferStream = new stream.PassThrough()
        bufferStream.end(file.data)
        file.name = invoiceNumber
        const driveFile = await driveService.createFile({
          folderId: driveFolder,
          fileObject: file,
          bufferStream
        })
        if (driveFile) {
          const sendFileData = JSON.parse(fileData)
          const createItems = {
            PL: () =>
              createPackingList({
                country: country.code || 'País Eliminado',
                brand: brand.reference || 'Marca Eliminada',
                sendFileData,
                packingListType,
                invoiceNumber,
                res
              }),
            Fact: () => createInvoiceItems(sendFileData),
            FactPDF: () => sendFileData,
            CO: () => [],
            Otros: () => []
          }
          const items = await createItems[data.type]()
          const { id, name } = driveFile
          data = {
            ...data,
            fileId: id,
            fileName: name,
            comexFile: !!comexFile,
            fileExtension,
            items,
            brand,
            country
          }
          const updateFile = await File.updateOne(
            {
              _id
            },
            data
          )
          if (updateFile) {
            const fileModelMessage = {
              PL: `Modificación de Lista de Empaque: ${data.fileName}`,
              CO: `Modificación de Certificado de Origen:  ${data.fileName}`,
              Fact: `Modificación de Factura: ${data.fileName}`,
              FactPDF: `Modificación de Factura en PDF: ${data.fileName}`,
              Otros: `Modificación de Otros documentos: ${data.fileName}`
            }
            const session = new Session({
              email: user.email,
              action: fileModelMessage[data.type],
              collectionName: 'File Model',
              user
            })

            dbService.createSesion(session)
            responseService.resultResponse({
              type: 'save',
              status: 201,
              res
            })
            if (data.type === 'PL') {
              sendCodesNotFound({
                packingList: items,
                user
              })
            }
          }
        } else {
          const senData = {
            message: 'Error al crear nuevo archivo, intente nuevamente',
            type: 'error'
          }
          responseService.resultResponse({
            type: 'custom',
            data: senData,
            status: 500,
            res
          })
        }
      } else {
        const senData = {
          message: 'Error al eliminar archivo, intente nuevamente',
          type: 'error'
        }
        responseService.resultResponse({
          type: 'custom',
          data: senData,
          status: 500,
          res
        })
      }
    } else {
      dbService.updateOneSession(
        File,
        'File',
        {
          _id
        },
        data,
        req,
        res
      )
    }
  } catch (err) {
    responseService.resultResponse({
      type: 'error',
      status: 500,
      data: err,
      res
    })
  }
}

/**
 * UPDATE PACKING LIST DATA
 *
 * @description :: UPDATE PACKING LIST DATA IN DB
 */
fileController.updateDocumentItems = async (req, res) => {
  const user = {
    ...req.user
  }
  try {
    const { _id } = req.params
    const { items, country, type, brand, packingListType, invoiceNumber } =
      req.body
    const documentData = JSON.parse(items)
    let documentItems = []
    if (type === 'PL') {
      documentItems = await createPackingList({
        sendFileData: documentData,
        country,
        packingListType,
        invoiceNumber,
        brand,
        res
      })
      sendCodesNotFound({
        documentItems,
        user
      })
    } else {
      documentItems = await createInvoiceItems(documentData)
    }
    dbService.updateOneSession(
      File,
      'File Model',
      {
        _id
      },
      {
        items: documentItems
      },
      req,
      res,
      'Actualización de Lista de Empaque'
    )
  } catch (err) {
    responseService.resultResponse({
      type: 'error',
      status: 500,
      data: err,
      res
    })
    const session = new Session({
      email: user.email,
      action: `Error al actualizar lista de empaque ${err.message}`,
      error: true,
      collectionName: 'File Model',
      user
    })
    dbService.createSesion(session)
  }
}

/**
 * UPDATE ALL UNCREATED SKU IN ALL PACKING LIST
 *
 * @description :: update uncreated sku in packing list item
 */
fileController.updateUncreatedSku = async (req, res) => {
  const user = {
    ...req.user
  }
  const body = {
    ...req.body
  }
  const { item } = body
  let { items } = body
  const bulkOps = []
  if (items) {
    items = JSON.parse(items)
  }
  if (item) {
    try {
      let resultsFind = []
      let updateFilter = {}
      let updateData = {}
      if (item.type === 'PL') {
        const referenceFind = await referenceModel
          .find({
            reference: item.reference,
            type: 'reference'
          })
          .exec()
        resultsFind = await extrenalService.getPaginateInfo({
          url: 'http://casos-amz.2becommerce.com/externalRequest/products',
          code: [item.code],
          country: item.country
        })
        updateFilter = {
          'items.code': item.code.trim(),
          'items.country': item.country,
          type: 'PL',
          $or: [
            {
              'items.incorrect': true
            },
            {
              'items.wrongReference': true
            }
          ]
        }
        updateData = {
          ...(resultsFind &&
            resultsFind.length > 0 && {
            'items.$.sku': resultsFind[0].sku.trim(),
            'items.$.incorrect': false
          }),
          ...(resultsFind &&
            resultsFind.length > 0 &&
            referenceFind &&
            referenceFind.length > 0 && {
            'items.$.wrongReference':
                referenceFind[0].code.trim() !== resultsFind[0].sku.trim()
          })
        }
      } else {
        resultsFind = await referenceModel
          .find({
            reference: item.reference,
            type: 'reference'
          })
          .exec()
        updateFilter = {
          'items.reference': item.reference,
          'items.incorrect': true,
          type: 'Fact'
        }
        updateData = {
          ...(resultsFind &&
            resultsFind.length > 0 && {
            'items.$.sku': resultsFind[0].code.trim(),
            'items.$.incorrect': false
          })
        }
      }

      if (resultsFind.length > 0) {
        File.updateMany(updateFilter, updateData)
          .exec()
          .then((updateFile) => {
            if (updateFile.nModified > 0) {
              const session = new Session({
                action: `Actualización de  (${updateFile.nModified}) Lista de Empaque (s)`,
                collectionName: 'File',
                email: user.email,
                user
              })
              dbService.createSesion(session)
              responseService.resultResponse({
                type: 'update',
                status: 200,
                res
              })
            } else {
              const err = {
                message:
                  'Ha ocurrido un error al eliminar los modificar de Base de Datos'
              }
              responseService.resultResponse({
                type: 'error',
                status: 500,
                data: err,
                res
              })
            }
          })
          .catch((err) => {
            responseService.resultResponse({
              type: 'error',
              status: 500,
              data: err,
              res
            })
          })
      } else {
        const err = {
          message: 'Sku no encontrado, para el código seleccionado'
        }
        responseService.resultResponse({
          type: 'error',
          status: 400,
          data: err,
          res
        })
      }
    } catch (err) {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    }
  } else if (items && items.length > 0) {
    async.each(
      items,
      async (data) => {
        let resultsFind = []
        let updateFilter = {}
        let updateData = {}
        if (data.type === 'PL') {
          const referenceFind = await referenceModel
            .find({
              reference: data.reference,
              type: 'reference'
            })
            .exec()
          resultsFind = await extrenalService.getPaginateInfo({
            url: 'http://casos-amz.2becommerce.com/externalRequest/products',
            code: [data.code],
            country: data.country
          })
          updateFilter = {
            'items.code': data.code,
            'items.country': data.country,
            type: 'PL',
            $or: [
              {
                'items.incorrect': true
              },
              {
                'items.wrongReference': true
              }
            ]
          }
          updateData = {
            ...(resultsFind &&
              resultsFind.length > 0 && {
              'items.$.sku': resultsFind[0].sku.trim(),
              'items.$.incorrect': false
            }),
            ...(resultsFind &&
              resultsFind.length > 0 &&
              referenceFind &&
              referenceFind.length > 0 && {
              'items.$.wrongReference':
                  referenceFind[0].code.trim() !== resultsFind[0].sku.trim()
            })
          }
        } else {
          // ACA QUEDE
          resultsFind = await referenceModel
            .find({
              reference: data.reference,
              type: 'reference'
            })
            .exec()
          updateFilter = {
            'items.reference': data.reference,
            'items.incorrect': true,
            type: 'Fact'
          }
          updateData = {
            'items.$.sku':
              resultsFind && resultsFind.length > 0
                ? resultsFind[0].code.trim()
                : '',
            'items.$.incorrect': false
          }
        }
        if (resultsFind.length > 0) {
          const upsertDoc = {
            updateMany: {
              filter: updateFilter,
              update: {
                $set: updateData
              }
            }
          }
          bulkOps.push(upsertDoc)
        }
      },
      (err) => {
        // if any of the file processing produced an error, err would equal that error
        if (err) {
          responseService.resultResponse({
            type: 'error',
            status: 500,
            data: err,
            res
          })
        } else if (bulkOps.length > 0) {
          File.collection
            .bulkWrite(bulkOps)
            .then(() => {
              responseService.resultResponse({
                type: 'save',
                status: 201,
                res
              })
            })
            .catch((err) => {
              responseService.resultResponse({
                type: 'error',
                status: 500,
                data: err,
                res
              })
            })
        } else {
          const err = {
            message:
              'No se han econtrado sku, para ninguno de los códigos seleccionados'
          }
          responseService.resultResponse({
            type: 'error',
            status: 400,
            data: err,
            res
          })
        }
      }
    )
  }
}
/**
 * DOWNLOAD DRIVE FILE
 *
 * @description :: DOWNLOAD DRIVE FILE
 */
fileController.donwload = async (req, res) => {
  try {
    const data = req.body
    const driveFile = await driveService.downloadFile(data.fileId)
    const fileName = data.fileName.replace(/[.,/ -]/g, '')
    if (driveFile) {
      const pathFile = path.resolve(
        __dirname,
        `../../static/files/${fileName}.${data.fileExtension}`
      ) // Path of file
      const document = fs.createWriteStream(pathFile)
      document.write(driveFile)
      req.pipe(document)
      document.on('finish', () => {
        res.download(pathFile, (err) => {
          if (err) {
            responseService.resultResponse({
              type: 'error',
              status: 500,
              data: err,
              res
            })
          } else if (fs.existsSync(pathFile)) {
            fs.unlinkSync(pathFile) // Delete the file
          }
        })
      })
    }
  } catch (err) {
    responseService.resultResponse({
      type: 'error',
      status: 500,
      data: err,
      res
    })
  }
}
/**
 * GET ALL
 *
 * @description :: list of all items in db
 */

fileController.getAll = (req, res) => {
  const body = req.body
  const { search, exportID, sortDirection, sortBy, comexFile } = body
  const currentPage = Number(body.currentPage)
  const limit = Number(body.perPage) || 50
  const skip = (currentPage - 1) * limit
  const searchName =
    search && search !== '' ? new RegExp(search.trim(), 'i') : ''
  const filter = {
    ...(searchName !== '' && {
      $or: [
        {
          invoiceNumber: searchName
        },
        {
          type: searchName
        }
      ]
    }),
    ...(exportID && {
      exportID
    }),
    ...(!comexFile && { comexFile: { $ne: true } })
  }

  File.countDocuments(filter)
    .then((filterRecord) => {
      const totalRows = filterRecord
      File.find(filter)
        .sort({
          [sortBy]: sortDirection
        })
        .skip(skip)
        .limit(limit)
        .lean()
        .exec()
        .then((results) => {
          async.each(
            results,
            async (exportFind) => {
              const filesWithError =
                await fileController.getFilesWithErrorsFunction({
                  exportID: exportFind.exportID,
                  invoiceNumber: exportFind.invoiceNumber
                })

              if (filesWithError && filesWithError.files.length > 0) {
                exportFind.wrongInvoice = true
                exportFind.filesWithError = filesWithError.filesWithErrors
              }
            },
            (err) => {
              // if any of the file processing produced an error, err would equal that error
              if (err) {
                res.json({
                  item: [],
                  totalRows
                })
              } else {
                res.json({
                  item: results,
                  totalRows
                })
              }
            }
          )
        })
        .catch((err) => {
          responseService.resultResponse({
            type: 'error',
            status: 500,
            data: err,
            res
          })
        })
    })
    .catch(err =>
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    )
}

/**
 * GET DIFFERENCES
 *
 * @description ::get the differences between packing list and Invoice
 */

fileController.getDifferences = (req, res) => {
  const { exportID } = req.body
  File.aggregate([
    {
      $match: {
        exportID: mongoose.Types.ObjectId(exportID),
        type: {
          $in: ['PL', 'Fact']
        }
      }
    },
    {
      $unwind: '$items'
    },
    {
      $group: {
        _id: '$items.sku',
        packingList: {
          $sum: {
            $cond: {
              if: {
                $eq: ['$type', 'PL']
              },
              then: '$items.quantity',
              else: 0
            }
          }
        },
        invoice: {
          $sum: {
            $cond: {
              if: {
                $eq: ['$type', 'Fact']
              },
              then: '$items.quantity',
              else: 0
            }
          }
        }
      }
    },
    {
      $project: {
        _id: 0,
        sku: '$_id',
        'Cantidad (Factura)': '$invoice',
        'Cantidad (Lista Empaque)': '$packingList',
        diferencia: {
          $cond: {
            if: {
              $eq: ['$invoice', '$packingList']
            },
            then: 'Cantidades iguales',
            else: 'Cantidades diferentes'
          }
        }
      }
    }
  ])
    .exec()
    .then((files) => {
      responseService.resultResponse({
        type: 'list',
        status: 200,
        data: files,
        res
      })
    })
    .catch(err =>
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    )
}

/**
 * GET DATA AND GROUP BY INVOICE NUMBER
 *
 * @description :: returns a list of files that have errors (ROUTE)

 */
fileController.getFilesWithErrors = (req, res) => {
  const { exportID } = req.params
  fileController
    .getFilesWithErrorsFunction({
      exportID
    })
    .then((files) => {
      responseService.resultResponse({
        type: 'list',
        status: 200,
        data: {
          files: files.files.map(x => x._id),
          packingList: files.packingList
        },
        res
      })
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

/**
 * GET DATA AND GROUP BY INVOICE NUMBER
 *
 * @description :: returns a list of files that have errors (FUNCTIONS)

 */
fileController.getFilesWithErrorsFunction = (objectParams) => {
  const { exportID, invoiceNumber } = objectParams
  const invoiceClear = invoiceNumber ? invoiceNumber.split(/[.,/ -]/) : null
  return new Promise((resolve, reject) => {
    File.aggregate([
      {
        $match: {
          exportID: mongoose.Types.ObjectId(exportID),
          type: {
            $nin: ['Otros', 'CO']
          },
          ...(invoiceNumber && {
            invoiceNumber: {
              $in: invoiceClear.map(x => new RegExp(`${x}.*`, 'i'))
            }
          })
        }
      },
      {
        $unwind: '$items'
      },
      {
        $group: {
          _id: '$_id',
          totalQtty: {
            $sum: '$items.quantity'
          },
          wrongSku: {
            $sum: {
              $cond: {
                if: {
                  $eq: ['$items.incorrect', true]
                },
                then: 1,
                else: 0
              }
            }
          },
          wrongQuantity: {
            $sum: {
              $cond: {
                if: {
                  $eq: ['$items.wrongQuantity', true]
                },
                then: 1,
                else: 0
              }
            }
          },
          wrongReference: {
            $sum: {
              $cond: {
                if: {
                  $eq: ['$items.wrongReference', true]
                },
                then: 1,
                else: 0
              }
            }
          },
          type: {
            $first: '$type'
          },
          invoiceNumber: {
            $first: '$invoiceNumber'
          },
          items: {
            $push: '$items'
          }
        }
      },
      {
        $group: {
          _id: {
            invoiceNumber: '$invoiceNumber',
            type: '$type'
          },
          total: {
            $sum: 1
          },
          files: {
            $last: {
              type: '$type',
              items: '$items',
              totalQtty: '$totalQtty',
              wrongSku: '$wrongSku',
              wrongReference: '$wrongReference',
              wrongQuantity: '$wrongQuantity'
            }
          }
        }
      },
      {
        $group: {
          _id: '$_id.invoiceNumber',
          total: {
            $sum: '$total'
          },
          files: {
            $push: '$files'
          }
        }
      }
    ])
      .exec()
      .then((files) => {
        // VALIDATION CASE 1 --- 2 INVOICES - 1 PACKING LIST 1 PDF INVOICE
        if (files.length > 0) {
          // find if there is at least one packing list in the list of documents
          const findPackingListFile = files.find(doc =>
            doc.files.find(file => file.type === 'PL')
          )
          // SAVE ERRORS IN ARRAY
          const filesWithErrors = []
          const wrongQuantity = files.filter(doc =>
            doc.files.some(file => file.wrongQuantity > 0)
          ) // WRONG QUANTITIES
          const wrongSkuArray = files.filter(
            doc =>
              doc.files.some(
                file => file.wrongSku > 0 || file.wrongReference > 0
              ) // WRONG SKU OR REFERENCE
          )
          if (wrongSkuArray.length > 0) {
            filesWithErrors.push({
              error: 'Sku o referencias incorrectas',
              items: wrongSkuArray.map(x => x._id)
            })
          }
          if (wrongQuantity.length > 0) {
            filesWithErrors.push({
              error: 'Cantidades incorrectas respecto a pedidos',
              items: wrongQuantity.map(x => x._id)
            })
          }
          // SEARCH FOR ALL FILES CONTAINING ONLY PACKING LIST AND PDF INVOICE AND NO ERROR
          const filterPackingListCaseOne = files.filter(
            doc =>
              !doc.files.some(
                file =>
                  file.type === 'Fact' ||
                  file.wrongSku > 0 ||
                  file.wrongReference > 0 ||
                  file.wrongQuantity > 0
              ) &&
              ['FactPDF', 'PL'].every(type =>
                doc.files.some(file => file.type === type)
              )
          )
          // VALIDATION CASE 2 -- 2 PACKING LIST - 1 INVOICE 1 CERTIFICATE OF ORIGIN
          // SEARCH FOR ALL FILES CONTAINING ONLY INVOICE AND CERTIFICATE OF ORIGIN AND NO ERROR
          const filterInvoiceListCaseTwo = files.filter(
            doc =>
              !doc.files.some(
                file =>
                  file.type === 'PL' ||
                  file.wrongSku > 0 ||
                  file.wrongReference > 0 ||
                  file.wrongQuantity > 0
              ) &&
              ['FactPDF', 'Fact'].every(type =>
                doc.files.some(file => file.type === type)
              )
          )
          const invoiceCaseOne = []
          const invoiceCaseTwo = []
          const invoicesWithAllFiles = []
          files.forEach((x) => {
            // VALIDATE BY NUMBER OF FILES AND TOTAL QTTY
            // stores in the array (invoicesWithAllFiles) all documents with 3 files and equal amounts
            if (x.total === 3) {
              let totalPackingList = 0
              let totalInvoice = 0
              const errorsInFile = []
              let plArray = []
              let invcArray = []
              x.files.forEach((doc) => {
                const error =
                  doc.wrongSku > 0 ||
                  doc.wrongReference > 0 ||
                  doc.wrongQuantity > 0
                if (error) {
                  errorsInFile.push(true)
                }
                if (doc.type === 'PL') {
                  totalPackingList = doc.totalQtty
                  plArray = [...doc.items]
                } else if (doc.type === 'Fact') {
                  totalInvoice = doc.totalQtty
                  invcArray = [...doc.items]
                }
              })
              const diferentArrays = plArray.filter(
                pl =>
                  !invcArray.some(
                    inv =>
                      pl.sku === inv.sku &&
                      pl.quantity === inv.quantity &&
                      pl.reference === inv.reference
                  )
              )
              if (diferentArrays.length > 0) {
                const index = filesWithErrors.findIndex(
                  x =>
                    x.error === 'Diferencias entre Lista de Empaque y Factura'
                )
                if (index !== -1) {
                  filesWithErrors[index].items.push(x._id)
                } else {
                  filesWithErrors.push({
                    error: 'Diferencias entre Lista de Empaque y Factura',
                    items: [x._id]
                  })
                }
              }
              if (
                totalInvoice === totalPackingList &&
                errorsInFile.length === 0 &&
                diferentArrays.length === 0
              ) {
                invoicesWithAllFiles.push(x._id)
              }
            } else {
              const index = filesWithErrors.findIndex(
                x => x.error === 'Documentos de exportacion incompletos'
              )
              if (index !== -1) {
                filesWithErrors[index].items.push(x._id)
              } else {
                filesWithErrors.push({
                  error: 'Documentos de exportacion incompletos',
                  items: [x._id]
                })
              }
            }

            // GROUP BY PACKING LIST NAME (CASE 1 --> 2 INVOICES 1 PACKING LIST)
            groupDocumentByName(
              x,
              filterPackingListCaseOne,
              'Fact',
              invoiceCaseOne
            )

            // GROUP BY PACKING Invoice NAME (CASE 2 --> 2 PACKING LIST 1 INVOICE)
            groupDocumentByName(
              x,
              filterInvoiceListCaseTwo,
              'PL',
              invoiceCaseTwo
            )
          })

          // REMOVE FROM FILE ARRAY THE ELEMENTS WITH A PACKING LIST AND TWO INVOICES
          if (invoiceCaseOne.length > 0) {
            files = [
              ...filterFiles(
                invoiceCaseOne,
                'PL',
                'Fact',
                files,
                filesWithErrors
              )
            ]
          }
          // REMOVE FROM FILE ARRAY THE ELEMENTS WITH A INVOICE AND TWO PACKING LIST
          if (invoiceCaseTwo.length > 0) {
            files = [
              ...filterFiles(
                invoiceCaseTwo,
                'Fact',
                'PL',
                files,
                filesWithErrors
              )
            ]
          }
          // REMOVE FROM FILE ARRAY (all documents with 3 files and equal amounts)
          files = files.filter(file => !invoicesWithAllFiles.includes(file._id))
          resolve({
            empty: false,
            packingList: !!findPackingListFile,
            files,
            filesWithErrors
          })
        } else {
          resolve({
            empty: true,
            packingList: false,
            files: []
          })
        }
      })
      .catch(err => reject(err))
  })
}

/**
 * GET UNCREATED SKU
 *
 * @description :: get all uncreated skus from packing list

 */
fileController.getUncreatedSku = (req, res) => {
  const body = req.body
  const { type } = req.query
  const { search, sortDirection, sortBy } = body
  const searchName =
    search && search !== '' ? new RegExp(search.trim(), 'i') : ''
  const currentPage = Number(body.currentPage)
  const limit = Number(body.perPage) || 50
  const skip = (currentPage - 1) * limit
  const match = {
    ...(searchName !== '' && {
      $or: [
        {
          'items.code': searchName
        },
        {
          'items.reference': searchName
        }
      ]
    }),
    type: {
      $in: ['PL', 'Fact']
    },
    $or: [
      {
        'items.incorrect': true
      },
      {
        'items.wrongReference': true
      }
    ]
  }
  File.aggregate([
    {
      $match: match
    },
    {
      $unwind: '$items'
    },
    {
      $match: match
    },
    {
      $group: {
        _id: {
          reference: '$items.reference',
          code: '$items.code',
          country: '$items.country',
          type: '$type'
        }
      }
    }
  ])
    .exec()
    .then((filterRecords) => {
      if (filterRecords && filterRecords.length > 0) {
        getFilterUncreatedSku({
          getExportNumber: !type,
          filterRecords: filterRecords.length,
          match,
          sortDirection,
          sortBy,
          limit,
          skip,
          res
        })
      } else {
        res.json({
          item: [],
          filterRecords: 0
        })
      }
    })
    .catch(err =>
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    )
}

/**
 * getSupplyInfo
 *
 * @description :: GET ALL PL GROUP BY EXPORTID FOR SUPPLY MODULE (PAGINATED)
 */
fileController.getSupplyInfo = async (req, res) => {
  try {
    const body = req.body
    const { exportID, countries, brands, status } = body
    const currentPage = Number(body.currentPage)
    const limit = Number(body.perPage) || 50
    const skip = (currentPage - 1) * limit
    let statusExport = []
    if (status) {
      const matchExportModule = {
        ...(exportID &&
          exportID.length > 0 && {
          _id: {
            $in: exportID.map(x => mongoose.Types.ObjectId(x._id))
          }
        }),
        ...(countries &&
          countries.length > 0 && {
          destinationCountry: {
            $in: countries.map(x => mongoose.Types.ObjectId(x._id))
          }
        }),
        ...(brands &&
          brands.length > 0 && {
          brand: {
            $in: brands.map(x => mongoose.Types.ObjectId(x._id))
          }
        }),
        orderStatus: status
      }
      const exportsFind = await exportModel
        .find(matchExportModule)
        .sort({ createdAt: -1 })
        .skip(skip)
        .limit(limit)
      statusExport = exportsFind.map(x => mongoose.Types.ObjectId(x._id))
    }
    const match = {
      ...(exportID &&
        exportID.length > 0 &&
        statusExport.length > 0 && {
        exportID: {
          $in: [
            ...exportID.map(x => mongoose.Types.ObjectId(x._id)),
            ...statusExport
          ]
        }
      }),
      ...(exportID &&
        exportID.length > 0 &&
        statusExport.length === 0 && {
        exportID: {
          $in: exportID.map(x => mongoose.Types.ObjectId(x._id))
        }
      }),
      ...(exportID &&
        exportID.length === 0 &&
        statusExport.length > 0 && {
        exportID: {
          $in: statusExport
        }
      }),
      ...(countries &&
        countries.length > 0 && {
        country: {
          $in: countries.map(x => mongoose.Types.ObjectId(x._id))
        }
      }),
      ...(brands &&
        brands.length > 0 && {
        brand: {
          $in: brands.map(x => mongoose.Types.ObjectId(x._id))
        }
      }),
      type: 'PL'
    }

    const fileRecords = await File.aggregate([
      {
        $match: match
      },
      {
        $group: {
          _id: '$exportID'
        }
      },
      {
        $group: { _id: null, count: { $sum: 1 } }
      }
    ]).exec()
    if (fileRecords && fileRecords.length > 0) {
      const totalRows = fileRecords[0].count
      const files = await File.aggregate([
        {
          $match: match
        },
        {
          $group: {
            _id: '$exportID',
            details: {
              $push: {
                invoiceNumber: '$invoiceNumber',
                createdAt: '$createdAt',
                fileId: '$fileId',
                fileName: '$fileName',
                fileExtension: '$fileExtension',
                packingListType: '$packingListType',
                items: '$items'
              }
            }
          }
        },
        {
          $sort: { 'details.createdAt': -1 }
        },
        {
          $skip: skip
        },
        {
          $limit: limit
        }
      ]).exec()
      if (files && files.length > 0) {
        const filesArray = []
        async.each(
          files,
          async (file) => {
            const detail = []
            const exportID = await exportModel
              .findOne({ _id: file._id })
              .populate([
                {
                  path: 'originCountry'
                },
                {
                  path: 'destinationCountry'
                },
                {
                  path: 'brand'
                }
              ])
              .lean()
              .exec()

            if (exportID) {
              file._id = exportID
              file.details.forEach((x) => {
                const items = x.items.map(item => ({
                  ...item,
                  invoiceNumber: x.invoiceNumber,
                  packingListType: x.packingListType,
                  fileId: x.fileId,
                  fileName: x.fileName,
                  fileExtension: x.fileExtension,
                  ...(exportID && { exportNumber: exportID.exportNumber })
                }))
                detail.push(...items)
              })
              file.details = [...detail]
              filesArray.push(file)
            }
          },
          (err) => {
            // if any of the file processing produced an error, err would equal that error
            if (err) {
              res.json({
                item: [],
                totalRows
              })
            } else {
              res.json({
                item: filesArray,
                totalRows
              })
            }
          }
        )
      } else {
        res.json({
          item: [],
          totalRows
        })
      }
    } else {
      res.json({
        item: [],
        totalRows: 0
      })
    }
  } catch (err) {
    responseService.resultResponse({
      type: 'error',
      status: 500,
      data: err,
      res
    })
  }
}

/**
 * getSupplyInfoExcel
 *
 * @description :: GET ALL PL ITEMS FOR SUPPLY MODULE (PAGINATED) (GENERATE AN EXCELFILE)
 * (Validate failed exports)
 */
fileController.getSupplyInfoExcel = async (req, res) => {
  try {
    const body = req.body
    const { exportID, countries, brands, status } = body
    const currentPage = Number(body.currentPage)
    const limit = Number(body.perPage) || 50
    const skip = (currentPage - 1) * limit
    let statusExport = []
    if (status) {
      const matchExportModule = {
        ...(exportID &&
          exportID.length > 0 && {
          _id: {
            $in: exportID.map(x => mongoose.Types.ObjectId(x._id))
          }
        }),
        ...(countries &&
          countries.length > 0 && {
          destinationCountry: {
            $in: countries.map(x => mongoose.Types.ObjectId(x._id))
          }
        }),
        ...(brands &&
          brands.length > 0 && {
          brand: {
            $in: brands.map(x => mongoose.Types.ObjectId(x._id))
          }
        }),
        orderStatus: status
      }
      const exportsFind = await exportModel
        .find(matchExportModule)
        .sort({ createdAt: -1 })
        .skip(skip)
        .limit(limit)
      statusExport = exportsFind.map(x => mongoose.Types.ObjectId(x._id))
    }

    const match = {
      ...(exportID &&
        exportID.length > 0 &&
        statusExport.length > 0 && {
        exportID: {
          $in: [
            ...exportID.map(x => mongoose.Types.ObjectId(x._id)),
            ...statusExport
          ]
        }
      }),
      ...(exportID &&
        exportID.length > 0 &&
        statusExport.length === 0 && {
        exportID: {
          $in: exportID.map(x => mongoose.Types.ObjectId(x._id))
        }
      }),
      ...(exportID &&
        exportID.length === 0 &&
        statusExport.length > 0 && {
        exportID: {
          $in: statusExport
        }
      }),
      ...(countries &&
        countries.length > 0 && {
        country: {
          $in: countries.map(x => mongoose.Types.ObjectId(x._id))
        }
      }),
      ...(brands &&
        brands.length > 0 && {
        brand: {
          $in: brands.map(x => mongoose.Types.ObjectId(x._id))
        }
      }),
      type: 'PL'
    }
    const totalFiles = await File.aggregate([
      {
        $match: match
      },
      {
        $unwind: '$items'
      },
      { $group: { _id: null, totalRows: { $sum: 1 } } }
    ]).exec()
    if (totalFiles) {
      const files = await File.aggregate([
        {
          $match: match
        },
        {
          $unwind: '$items'
        },
        {
          $project: {
            _id: 0,
            'NRO DE EXPORTACION': '$exportID',
            'NRO DE DOCUMENTO': '$invoiceNumber',
            SKU: '$items.sku',
            DESPACHO: '$items.quantity',
            'PEDIDO A LA MARCA': '$items.orderQuantity',
            DIFERENCIA: {
              $subtract: ['$items.quantity', '$items.orderQuantity']
            }
          }
        },
        {
          $skip: skip
        },
        {
          $limit: limit
        }
      ]).exec()

      const exportArray = []
      const fileErrors = []
      if (files && files.length > 0) {
        async.each(
          files,
          async (file) => {
            const findID = exportArray.find(
              x => x._id === file['NRO DE EXPORTACION']
            )
            if (!findID) {
              const exportID = await exportModel
                .findOne({ _id: file['NRO DE EXPORTACION'] })
                .lean()
                .exec()

              if (exportID) {
                exportArray.push(exportID)
                file['NRO DE EXPORTACION'] = exportID.exportNumber
              }
            } else {
              file['NRO DE EXPORTACION'] = findID.exportNumber
            }
          },
          (err) => {
            // if any of the file processing produced an error, err would equal that error
            if (err) {
              res.json({
                item: [],
                totalRows: totalFiles[0].totalRows
              })
            } else {
              res.json({
                item: files.filter(
                  x => !fileErrors.includes(x['NRO DE EXPORTACION'])
                )
              })
            }
          }
        )
      } else {
        res.json({
          item: [],
          totalRows: 0
        })
      }
    } else {
      res.json({
        item: [],
        totalRows: 0
      })
    }
  } catch (err) {
    responseService.resultResponse({
      type: 'error',
      status: 500,
      data: err,
      res
    })
  }
}

/**
 * getSupplyInfoExcel
 *
 * @description :: GET ALL PL ITEMS FOR SUPPLY MODULE (PAGINATED) (GENERATE AN EXCELFILE)
 * (Validate failed exports)
 */
fileController.getWarehouseInfo = async (req, res) => {
  try {
    const body = req.body
    const { exportID, countries, brands, status } = body
    const currentPage = Number(body.currentPage)
    const limit = Number(body.perPage) || 50
    const skip = (currentPage - 1) * limit
    let statusExport = []
    if (status) {
      const matchExportModule = {
        ...(exportID &&
          exportID.length > 0 && {
          _id: {
            $in: exportID.map(x => mongoose.Types.ObjectId(x._id))
          }
        }),
        ...(countries &&
          countries.length > 0 && {
          destinationCountry: {
            $in: countries.map(x => mongoose.Types.ObjectId(x._id))
          }
        }),
        ...(brands &&
          brands.length > 0 && {
          brand: {
            $in: brands.map(x => mongoose.Types.ObjectId(x._id))
          }
        }),
        orderStatus: status
      }
      const exportsFind = await exportModel
        .find(matchExportModule)
        .sort({ createdAt: -1 })
        .skip(skip)
        .limit(limit)
      statusExport = exportsFind.map(x => mongoose.Types.ObjectId(x._id))
    }
    const match = {
      ...(exportID &&
        exportID.length > 0 &&
        statusExport.length > 0 && {
        exportID: {
          $in: [
            ...exportID.map(x => mongoose.Types.ObjectId(x._id)),
            ...statusExport
          ]
        }
      }),
      ...(exportID &&
        exportID.length > 0 &&
        statusExport.length === 0 && {
        exportID: {
          $in: exportID.map(x => mongoose.Types.ObjectId(x._id))
        }
      }),
      ...(exportID &&
        exportID.length === 0 &&
        statusExport.length > 0 && {
        exportID: {
          $in: statusExport
        }
      }),
      ...(countries &&
        countries.length > 0 && {
        country: {
          $in: countries.map(x => mongoose.Types.ObjectId(x._id))
        }
      }),
      ...(brands &&
        brands.length > 0 && {
        brand: {
          $in: brands.map(x => mongoose.Types.ObjectId(x._id))
        }
      }),
      type: 'PL'
    }
    const fileRecords = await File.aggregate([
      {
        $match: match
      },
      {
        $group: {
          _id: '$exportID'
        }
      },
      {
        $group: { _id: null, count: { $sum: 1 } }
      }
    ]).exec()
    if (fileRecords && fileRecords.length > 0) {
      const totalRows = fileRecords[0].count
      const files = await File.aggregate([
        {
          $match: match
        },
        { $unwind: '$items' },
        { $sort: { updatedAt: -1 } },
        {
          $group: {
            _id: { exportID: '$exportID', box: '$items.box' },
            quantity: {
              $sum: '$items.quantity'
            },
            updatedAt: { $first: '$updatedAt' }
          }
        },
        { $sort: { updatedAt: -1 } },
        {
          $group: {
            _id: '$_id.exportID',
            total: {
              $sum: '$quantity'
            },
            box: { $sum: 1 },
            updatedAt: { $first: '$updatedAt' }
          }
        },
        { $sort: { updatedAt: -1 } },
        {
          $skip: skip
        },
        {
          $limit: limit
        }
      ]).exec()
      if (files && files.length > 0) {
        const fileItems = []
        async.eachSeries(
          files,
          async (file) => {
            const exportID = await exportModel
              .findOne({ _id: file._id })
              .populate([
                {
                  path: 'originCountry'
                },
                {
                  path: 'destinationCountry'
                },
                {
                  path: 'brand'
                }
              ])
              .lean()
              .exec()

            if (exportID) {
              file._id = exportID
              fileItems.push({ ...file })
            }
          },
          (err) => {
            // if any of the file processing produced an error, err would equal that error
            if (err) {
              res.json({
                item: [],
                totalRows
              })
            } else {
              res.json({
                item: fileItems,
                totalRows
              })
            }
          }
        )
      } else {
        res.json({
          item: [],
          totalRows
        })
      }
    } else {
      res.json({
        item: [],
        totalRows: 0
      })
    }
  } catch (err) {
    responseService.resultResponse({
      type: 'error',
      status: 500,
      data: err,
      res
    })
  }
}
/**
 * updateorderQuantity
 *
 * @description :: UPDATE IN BULK ORDERS QUANTITY IN PACKING LIST FILE
 */
fileController.updateorderQuantity = async (req, res) => {
  try {
    const user = { ...req.user }
    const body = req.body
    const { exportID } = body
    const items = JSON.parse(body.items)
    const invoiceNumbers = [...new Set(items.map(x => x.invoiceNumber))]
    const updateOptions = []
    const token = await dbService.getRequest({
      method: 'POST',
      url: 'http://pedidos.2becommerce.com:3000/login',
      data: {
        email: pedidosEmail,
        password: pedidosPass
      }
    })
    for (const invoiceNumber of invoiceNumbers) {
      const packingListItems = []
      const filterItems = items.filter(x => x.invoiceNumber === invoiceNumber)
      let orderQuantities = []
      if (token) {
        if (filterItems[0].packingListType === 'Especial') {
          orderQuantities = await getOrderDetail({
            token: token.token,
            country: filterItems[0].country,
            invoiceNumber
          })
        } else {
          orderQuantities = await getOrderDetailLine({
            token: token.token,
            country: filterItems[0].country,
            brand: exportID.brand.code || '-'
          })
        }
        filterItems.forEach((item) => {
          const orderFind = orderQuantities.find(
            order => order.sku === item.sku
          )
          const orderQuantity = orderFind ? orderFind.total : 0
          delete item.invoiceNumber
          delete item.packingListType
          packingListItems.push({
            ...item,
            ...(item.packingListType === 'Especial'
              ? { wrongQuantity: item.quantity !== orderQuantity }
              : { wrongQuantity: item.quantity > orderQuantity }),
            difference: Number(item.quantity) - Number(orderQuantity),
            orderQuantity
          })
        })
        updateOptions.push({
          updateOne: {
            filter: {
              type: 'PL',
              exportID: mongoose.Types.ObjectId(exportID._id),
              invoiceNumber
            },
            update: {
              $set: {
                items: packingListItems
              }
            },
            upsert: false
          }
        })
      }
    }
    const fileUpdate = await File.collection.bulkWrite(updateOptions)
    if (fileUpdate) {
      const session = new Session({
        action: 'Actualización de cantidades de pedidos en Lista de Empaque',
        collectionName: 'File',
        email: user.email,
        user
      })
      dbService.createSesion(session)
      responseService.resultResponse({
        type: 'update',
        status: 200,
        res
      })
    }
  } catch (err) {
    responseService.resultResponse({
      type: 'error',
      status: 500,
      data: err,
      res
    })
  }
}
/**
 * DELETE ITEM
 *
 * @description :: DELETE ONE ITEM IN DB
 */

fileController.delete = async (req, res) => {
  try {
    const { _id } = req.params
    const { fileId } = req.body
    const deleteFile = await driveService.delete(fileId)
    if (deleteFile) {
      dbService.deletePhysicalSession(
        File,
        'File',
        {
          _id
        },
        req,
        res
      )
    }
  } catch (err) {
    responseService.resultResponse({
      type: 'error',
      status: 500,
      data: err,
      res
    })
  }
}
// ============================================== FUNCTIONS ===================================================

/**
 * CREATE PACKING LIST FUNCTION
 * @param {*} objectPackingList :: {sendFileData, country }
 * @description :: Search for sku in the logistics application for packing list creation
 */
function createPackingList (objectPackingList) {
  const { sendFileData, country, packingListType, invoiceNumber, brand } =
    objectPackingList
  return new Promise((resolve, reject) => {
    ;(async () => {
      try {
        let orderQuantities = []
        if (packingListType) {
          const token = await dbService.getRequest({
            method: 'POST',
            url: 'http://pedidos.2becommerce.com:3000/login',
            data: {
              email: pedidosEmail,
              password: pedidosPass
            }
          })
          if (token) {
            if (packingListType === 'Especial') {
              orderQuantities = await getOrderDetail({
                token: token.token,
                invoiceNumber,
                country
              })
            } else {
              orderQuantities = await getOrderDetailLine({
                token: token.token,
                brand,
                country
              })
            }
          }
        }
        const references = sendFileData.map(x => x.reference.toString().trim())
        const referenceFind = await referenceModel
          .find({
            reference: {
              $in: references
            },
            type: 'reference'
          })
          .exec()

        // ACÁ QUÉ (RECORDAR VALIDAR REFERENCIAS DE PACKING LIST CON INFO EN BD)
        let packingList = []
        const code = sendFileData.map(x => x.code.trim())
        const resultsFind = await extrenalService.getPaginateInfo({
          url: 'http://casos-amz.2becommerce.com/externalRequest/products',
          code,
          country
        })
        if (resultsFind.length > 0) {
          sendFileData.forEach((item) => {
            const product = resultsFind.find(
              x => x.upc.trim() === item.code.trim()
            )
            if (product) {
              const sku = product.sku.trim()
              const orderFind = orderQuantities.find(order => order.sku === sku)
              const orderQuantity = orderFind ? orderFind.total : 0
              const findCorrectReference = referenceFind.find(
                x =>
                  x.code.trim() === sku &&
                  x.reference.trim() === item.reference.trim()
              )
              packingList.push({
                ...item,
                sku,
                incorrect: false,
                wrongReference: !findCorrectReference,
                ...(packingListType === 'Especial'
                  ? { wrongQuantity: item.quantity !== orderQuantity }
                  : { wrongQuantity: item.quantity > orderQuantity }),
                difference: Number(item.quantity) - Number(orderQuantity),
                orderQuantity,
                country
              })
            } else {
              const findCorrectReference = referenceFind.find(
                x => x.reference.trim() === item.reference.trim()
              )
              packingList.push({
                ...item,
                sku: findCorrectReference
                  ? findCorrectReference.code.trim()
                  : 'SIN CREAR',
                incorrect: true,
                wrongReference: !findCorrectReference,
                wrongQuantity: true,
                difference: Number(item.quantity),
                country
              })
            }
          })
        } else {
          packingList = sendFileData.map((x) => {
            const findCorrectReference = referenceFind.find(
              ref => ref.code.trim() === x.reference.trim()
            )
            return {
              ...x,
              sku: findCorrectReference
                ? findCorrectReference.code.trim()
                : 'SIN CREAR',
              incorrect: true,
              wrongReference: true,
              wrongQuantity: true,
              difference: 0,
              country
            }
          })
        }
        resolve(packingList)
      } catch (err) {
        reject(err)
      }
    })()
  })
}

/**
 * CREATE INVOICE ITEMS FUNCTION
 * @param {*} invoiceItems :: invoice items (ARRAY)
 * @description :: Search for sku in REFERENCE MODEL
 */
function createInvoiceItems (invoiceItems) {
  return new Promise((resolve, reject) => {
    ;(async () => {
      try {
        let invoice = []
        const code = invoiceItems.map(x => x.reference.toString().trim())
        const referenceFind = await referenceModel
          .find({
            reference: {
              $in: code
            },
            type: 'reference'
          })
          .exec()
        if (referenceFind.length > 0) {
          invoiceItems.forEach((item) => {
            const reference = referenceFind.find(
              x =>
                x.reference &&
                x.reference.toString().trim() ===
                  item.reference.toString().trim()
            )
            if (reference) {
              invoice.push({
                ...item,
                reference: item.reference.toString().trim(),
                sku: reference.code,
                incorrect: false
              })
            } else {
              invoice.push({
                ...item,
                reference: item.reference.toString().trim(),
                sku: 'SIN CREAR',
                incorrect: true
              })
            }
          })
        } else {
          invoice = invoiceItems.map(x => ({
            ...x,
            reference: x.reference ? x.reference.toString().trim() : '',
            sku: 'SIN CREAR',
            incorrect: true
          }))
        }
        resolve(invoice)
      } catch (err) {
        reject(err)
      }
    })()
  })
}
/**
 * SEND CODES NOT FOUND
 * @description Send two files, one with the items that do not have sku, and another with the correct packing list
 * @param {*} packingListObject :: { packingList, user }
 */
async function sendCodesNotFound (packingListObject) {
  const { packingList, user } = packingListObject
  try {
    const codesNotFound = packingList.filter(x => x.incorrect)
    const codesFound = packingList.filter(x => !x.incorrect)

    if (codesNotFound.length > 0) {
      await sendEmail(
        codesNotFound,
        'codesNotFound.xlsx',
        'Packing List',
        'Listado de UPC no encontrados'
      )
    }
    if (codesFound.length > 0) {
      await sendEmail(
        codesFound,
        'packingList.xlsx',
        'Packing List',
        'Lista de Empaque'
      )
    }
  } catch (err) {
    const session = new Session({
      email: user.email,
      action: `Error enviar correo electrónico ${err.message}`,
      error: true,
      collectionName: 'File Model',
      user
    })
    dbService.createSesion(session)
  }
}

/**
 *
 * @param {*} items
 * @param {*} fileName
 * @param {*} fileType :: name of the file to search in database
 * @param {*} emailSubject
 * @description send a file with the packing list to the logged user
 */
async function sendEmail (items, fileName, fileType, emailSubject) {
  const templateFind = await templateModel
    .findOne({
      title: 'Email Packing List'
    })
    .exec()
  if (templateFind) {
    const pathTemplateFile = path.resolve(
      __dirname,
      `../../static/${templateFind.fileName}`
    )
    if (fs.existsSync(pathTemplateFile)) {
      const workbook = new Excel.Workbook()
      const readFile = await workbook.xlsx.readFile(pathTemplateFile) // FIND TEMPLATE

      if (readFile) {
        const worksheet = workbook.getWorksheet(1) // GET FIRST SHEET IN EXCEL FILE
        worksheet.columns = templateFind.columns.map((x, index) => ({
          // MAP COLUMNS IN FILE
          header: x,
          key: templateFind.translateColumns[index],
          width: 40
        }))
        worksheet.addRows(items) // ADD ALL ROWS
        const pathNewExcelFile = path.resolve(
          // CREATE FILE
          __dirname,
          `../../static/${fileName}`
        )
        await workbook.xlsx.writeFile(pathNewExcelFile)

        const emails = await emailModel
          .findOne({
            type: fileType
          })
          .exec()

        if (emails) {
          const smtpTransport = nodemailer.createTransport(transport)
          const mailOptions = {
            to: emails.emails,
            subject: emailSubject,
            attachments: [
              {
                filename: fileName,
                path: pathNewExcelFile
              }
            ]
          }
          smtpTransport.sendMail(mailOptions, (err) => {
            if (!err) {
              if (fs.existsSync(pathNewExcelFile)) {
                fs.unlinkSync(pathNewExcelFile)
              }
            }
          })
        }
      }
    }
  }
}

/**
 * @param {*} actualIndex
 * @param {*} filterDocument
 * @param {*} fileType
 * @param {*} invoiceArray
 * @description :: Group invoice array by name (Case 1 or Case 2)
 */
function groupDocumentByName (
  actualIndex,
  filterDocument,
  fileType,
  invoiceArray
) {
  const invoiceNumber = filterDocument.find(packingList =>
    packingList._id.toUpperCase().includes(actualIndex._id)
  )
  if (
    invoiceNumber &&
    actualIndex.files.find(file => file.type === fileType) &&
    actualIndex.total === 1
  ) {
    const invoiceNumberString = invoiceNumber._id
      .replace(/[^a-zA-Z0-9 ]/g, '')
      .replace(/ /g, '')
      .toUpperCase()
    const invoiceIndex = invoiceArray.findIndex(
      invoiceFind => invoiceFind.invoiceNumber === invoiceNumberString
    )
    if (invoiceIndex === -1) {
      invoiceArray.push({
        invoiceNumber: invoiceNumberString,
        files: invoiceNumber.files,
        invoices: [
          {
            ...actualIndex
          }
        ]
      })
    } else {
      invoiceArray[invoiceIndex].invoices.push({
        ...actualIndex
      })
    }
  }
}

/**
 *
 * @param {*} invoiceArray :: Filter invoice array (case 1 or case 2)
 * @param {*} fileType
 * @param {*} compareFileType
 * @param {*} files :: ARRAY OF FILES
 * @returns ARRAY WITH FILTER elements
 */
function filterFiles (
  invoiceArray,
  fileType,
  compareFileType,
  files,
  filesWithErrors
) {
  let filesArray = [...files]
  invoiceArray.forEach((x) => {
    const documentFind = x.files.find(doc => doc.type === fileType) || {}
    const skusDocument = documentFind.items.map(file => ({
      sku: file.sku,
      reference: file.reference,
      quantity: file.quantity
    }))
    const compareSkuDocument = []
    let concatInvoiceNumber = ''
    const invoicesNumbers = []
    let totalInvoiceQtty = 0
    x.invoices.forEach((invoice) => {
      const invoiceFind = invoice.files.find(
        doc =>
          doc.type === compareFileType && !doc.wrongSku && !doc.wrongReference
      )
      if (invoiceFind) {
        totalInvoiceQtty += invoiceFind.totalQtty || 0
        invoiceFind.items.forEach((invoiceItem) => {
          const compareSkuIndex = compareSkuDocument.findIndex(
            compare =>
              compare.sku === invoiceItem.sku &&
              compare.reference === invoiceItem.reference
          )
          if (compareSkuIndex !== -1) {
            compareSkuDocument[compareSkuIndex].quantity += invoiceItem.quantity
          } else {
            compareSkuDocument.push({
              sku: invoiceItem.sku,
              reference: invoiceItem.reference,
              quantity: invoiceItem.quantity
            })
          }
        })
      }
      concatInvoiceNumber += invoice._id
        .replace(/[^a-zA-Z0-9 ]/g, '')
        .replace(/ /g, '')
        .toUpperCase()
      invoicesNumbers.push(invoice._id)
    })
    const equalsArrays = skusDocument.filter(skuDoc =>
      compareSkuDocument.some(
        compareSkuDoc =>
          skuDoc.sku === compareSkuDoc.sku &&
          skuDoc.quantity === compareSkuDoc.quantity &&
          skuDoc.reference === compareSkuDoc.reference
      )
    )
    if (
      concatInvoiceNumber.length === x.invoiceNumber.length &&
      documentFind.totalQtty === totalInvoiceQtty &&
      equalsArrays.length === skusDocument.length
    ) {
      filesArray = filesArray.filter(
        file =>
          file._id
            .replace(/[^a-zA-Z0-9 ]/g, '')
            .replace(/ /g, '')
            .toUpperCase() !== x.invoiceNumber &&
          !invoicesNumbers.includes(file._id)
      )
    } else {
      const index = filesWithErrors.findIndex(
        x => x.error === 'Error en nombre de archivo o en cantidades'
      )
      if (index !== -1) {
        filesWithErrors[index].items.push(
          x.invoiceNumber,
          ...x.invoices.map(invoice => invoice._id)
        )
      } else {
        filesWithErrors.push({
          error: 'Error en nombre de archivo o en cantidades',
          items: [x.invoiceNumber, ...x.invoices.map(invoice => invoice._id)]
        })
      }
    }
  })
  return filesArray
}

/**
 *
 * @param {*} filterObject  :: object with all params
 * {
 * match :: Filters the documents to pass only the documents that match the specified condition
 * sortDirection :: direction of sort (asc - desc)
 * sortBy :: query sorted by field name
 * limit :: query limit
 * skip :: takes a positive integer that specifies the maximum number of documents to skip.
 * getExportNumber ::show export number or not
 * filterRecords:: number of filtered records
 * res
 * }
 */
function getFilterUncreatedSku (filterObject) {
  const {
    match,
    sortDirection,
    sortBy,
    limit,
    skip,
    getExportNumber,
    filterRecords,
    res
  } = filterObject

  File.aggregate([
    {
      $match: match
    },
    {
      $unwind: '$items'
    },
    {
      $match: match
    },
    {
      $sort: {
        [`items.${sortBy}`]: sortDirection === 'asc' ? 1 : -1
      }
    },
    {
      $group: {
        _id: {
          code: '$items.code',
          reference: '$items.reference',
          country: '$items.country',
          type: '$type'
        },
        details: {
          $addToSet: {
            invoiceNumber: '$invoiceNumber',
            exportID: '$exportID'
          }
        }
      }
    },
    {
      $project: {
        _id: 0,
        reference: '$_id.reference',
        code: '$_id.code',
        country: '$_id.country',
        type: '$_id.type',
        ...(getExportNumber
          ? {
              details: 1
            }
          : '')
      }
    },
    {
      $skip: skip
    },
    {
      $limit: limit
    }
  ])
    .exec()
    .then(async (files) => {
      if (files.length > 0 && getExportNumber) {
        const exports = []
        for (const file of files) {
          for (const detail of file.details) {
            try {
              const exportFind = exports.find(x => x._id === detail.exportID)
              if (exportFind) {
                detail.export = exportFind.exportNumber
              } else {
                const exportValue = await exportModel
                  .findOne({
                    _id: detail.exportID
                  })
                  .exec()
                if (exportValue) {
                  exports.push({
                    ...exportValue
                  })
                  detail.export = exportValue.exportNumber
                }
              }
            } catch {}
          }
        }
      }
      res.json({
        item: files,
        ...(filterRecords
          ? {
              totalRows: filterRecords
            }
          : '')
      })
    })
    .catch(err =>
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    )
}

/**
 * getOrderDetail :: { token, invoiceNumber, country }
 * @param {*} paramObject
 * @returns array of order details
 */
async function getOrderDetail (paramObject) {
  try {
    let orderQuantities = []
    const { token, invoiceNumber, country } = paramObject
    const orderId = await dbService.getRequest({
      method: 'POST',
      url: 'http://pedidos.2becommerce.com:3000/orderPQC/',
      headers: {
        token
      },
      data: {
        mark: null,
        order: invoiceNumber.trim(),
        state: null,
        country
      }
    })
    if (orderId && orderId.orders) {
      const orderDetail = await dbService.getRequest({
        method: 'GET',
        url: `http://pedidos.2becommerce.com:3000/orderPQC/${orderId.orders[0]._id}`,
        headers: {
          token
        }
      })
      if (orderDetail && orderDetail.detail) {
        orderQuantities = orderDetail.detail
      }
    }
    return orderQuantities
  } catch (err) {
    return []
  }
}

/**
 * getOrderDetailLine :: { token, brand, country }
 * @param {*} paramObject
 * @returns array of order details type Line
 */
async function getOrderDetailLine (paramObject) {
  try {
    let orderQuantities = []
    const { country, brand, token } = paramObject
    const orderDetail = await dbService.getRequest({
      method: 'GET',
      url: `http://pedidos.2becommerce.com:3000/generalReports/pendingBrandSku/${brand.toUpperCase()}/${country}`,
      headers: {
        token
      }
    })
    if (orderDetail && orderDetail.data) {
      orderQuantities = orderDetail.data
    }
    return orderQuantities
  } catch (err) {
    return []
  }
}
module.exports = fileController
