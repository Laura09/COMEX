const ChatRoom = require('../models/chatRoomModel')
const dbService = require('../services/dbService')

const chatRoomController = {}

/**
 * CREATE
 *
 * @description :: CREATE ITEM IN DB
 */
chatRoomController.create = (req, res) => {
  const data = req.body
  dbService.createOneSession(ChatRoom, 'ChatRoom', data, req, res)
}

/**
 * GET ONE
 *
 * @description :: GET ONE ITEM
 */

chatRoomController.getOne = (req, res) => {
  const { name } = req.params
  dbService.getOnePopulate(ChatRoom, { name }, '', res)
}

/**
 * GET ALL
 *
 * @description :: list of all items in db
 */

chatRoomController.getAll = (req, res) => {
  dbService.getAllParams({ model: ChatRoom, params: {}, res })
}

/**
 * UPDATE ITEM
 *
 * @description :: UPDATE ONE ITEM IN DB
 */

chatRoomController.update = (req, res) => {
  const { _id } = req.params
  dbService.updateOneSession(
    ChatRoom,
    'ChatRoom',
    {
      _id
    },
    req.body,
    req,
    res
  )
}

/**
 * DELETE ITEM
 *
 * @description :: DELETE ONE ITEM IN DB
 */

chatRoomController.delete = (req, res) => {
  const { _id } = req.params
  dbService.deletePhysicalSession(
    ChatRoom,
    'ChatRoom',
    {
      _id
    },
    req,
    res
  )
}

module.exports = chatRoomController
