const jwt = require('jsonwebtoken')
const User = require('../models/userModel')
const responseService = require('../services/responseService')
const authController = {}

/**
 *
 * @description :: USER LOGIN
 */

authController.login = (req, res) => {
  const body = req.body
  User.findOne({ email: RegExp(`^${body.email.trim()}$`, 'i') })
    .exec()
    .then((user) => {
      // if no user is found, return the message
      if (!user) {
        return responseService.resultResponse({
          type: 'error',
          status: 400,
          data: { message: 'Usuario Incorrecto' },
          res
        })
      }
      if (!user.validPassword(body.password)) {
        return responseService.resultResponse({
          type: 'error',
          status: 400,
          data: { message: 'Contraseña Incorrecta' },
          res
        })
      }
      // all is well, return user
      const token = jwt.sign({ user }, process.env.SEED, {})
      res.json({ ok: true, token, user })
    })
    .catch((err) => {
      return responseService.resultResponse({
        type: 'error',
        status: 500,
        data: { message: err },
        res
      })
    })
}

/**
 *
 * @description :: USER DETAIL
 */
authController.userDetail = (req, res) => {
  res.json(req.user)
}

module.exports = authController
