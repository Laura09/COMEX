const mongoose = require('mongoose')
const Privilege = require('../models/privilegeModel')
const Session = require('../models/sessionModel')
const dbService = require('../services/dbService')
const responseService = require('../services/responseService')

const privilegeController = {}

/**
 * CREATE
 *
 * @description :: CREATE ITEM IN DB
 */
privilegeController.create = (req, res) => {
  const data = req.body
  dbService.createOneSession(Privilege, 'Privilege', data, req, res)
}

/**
 * GET ALL
 *
 * @description :: list of all items in db
 */

privilegeController.getAll = (req, res) => {
  dbService.getAllParams({
    model: Privilege,
    params: {},
    populate: 'parent',
    res
  })
}

/**
 * GET PARENTS
 *
 * @description :: list of all items in db
 */

privilegeController.getParents = (req, res) => {
  dbService.getAllParams({
    model: Privilege,
    params: {
      parent: null
    },
    populate: {
      path: 'children._id',
      populate: {
        path: 'children._id'
      }
    },
    sort: {
      position: 1
    },
    res
  })
}

/**
 * GET BY ROLE
 *
 * @description :: list of all items in db
 */

privilegeController.getByRole = (req, res) => {
  const { role } = req.params
  dbService.getAllParams({
    model: Privilege,
    params: {
      'role._id': role
    },
    sort: {
      position: 1
    },
    res
  })
}

/**
 * UPDATE ITEM
 *
 * @description :: UPDATE ONE ITEM IN DB
 */

privilegeController.update = (req, res) => {
  const { _id } = req.params
  const body = req.body
  const user = req.user
  const { parent, changeParent } = body
  Privilege.updateOne(
    {
      _id
    },
    body
  )
    .then(async (modelSave) => {
      if (modelSave.nModified > 0) {
        const session = new Session({
          action: 'Actualización de registro',
          collectionName: 'Privilege',
          email: user.email,
          user
        })
        dbService.createSesion(session)
        responseService.resultResponse({
          type: 'update',
          status: 200,
          res
        })
        if (changeParent) {
          try {
            const updateChildrens = await Privilege.updateOne(
              {
                'children._id': _id
              },
              {
                $pull: {
                  children: {
                    _id
                  }
                }
              },
              {
                upsert: false
              }
            ).exec()
            if (updateChildrens && parent) {
              await Privilege.updateOne(
                {
                  _id: parent._id
                },
                {
                  $addToSet: {
                    children: {
                      _id
                    }
                  }
                },
                {
                  upsert: false
                }
              ).exec()
            }
          } catch (err) {
            console.log('err in privilege childs', err)
          }
        }
      } else {
        const err = {
          status: 'Modificar',
          message:
            'Ha ocurrido un error al guardar los cambios en base de datos'
        }
        responseService.resultResponse({
          type: 'error',
          status: 500,
          data: err,
          res
        })
      }
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

/**
 * UPDATE ROLES IN PRIVILEGES
 *
 * @description :: UPDATE ONE ITEM IN DB
 */

privilegeController.updateRoles = (req, res) => {
  const user = req.user
  let privilege = JSON.parse(req.body.data)
  privilege = privilege.map(x => ({
    updateOne: {
      filter: { _id: mongoose.Types.ObjectId(x._id) },
      update: {
        ...(x.pull
          ? {
              $pull: {
                role: {
                  _id: mongoose.Types.ObjectId(x.pull._id),
                  role: x.pull.role
                }
              }
            }
          : {
              $addToSet: {
                role: {
                  _id: mongoose.Types.ObjectId(x.addToSet._id),
                  role: x.addToSet.role
                }
              }
            })
      }
    }
  }))
  Privilege.collection
    .bulkWrite(privilege)
    .then((bulkWriteOpResult) => {
      const session = new Session({
        action: 'Actualización de registro',
        collectionName: 'Privilege',
        email: user.email,
        user
      })
      dbService.createSesion(session)
      responseService.resultResponse({
        type: 'update',
        status: 200,
        res
      })
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

/**
 * DELETE ITEM
 *
 * @description :: DELETE ONE ITEM IN DB
 */

privilegeController.delete = (req, res) => {
  const { _id } = req.params
  dbService.deletePhysicalSession(
    Privilege,
    'Privilege',
    {
      _id
    },
    req,
    res
  )
}

module.exports = privilegeController
