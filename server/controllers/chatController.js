const mongoose = require('mongoose')
const ChatModel = require('../models/chatModel')
const dbService = require('../services/dbService')
const responseService = require('../services/responseService')

const chatController = {}

/**
 * CREATE
 *
 * @description :: CREATE ITEM IN DB
 */
chatController.create = (req, res) => {
  const data = req.body
  dbService.createOne(ChatModel, data, res)
}

/**
 * GET ALL
 *
 * @description :: list of all items in db
 */

chatController.getAll = (req, res) => {
  const body = req.body
  const { chatRoom, email } = body
  const currentPage = Number(body.currentPage)
  const limit = Number(body.perPage) || 50
  const skip = (currentPage - 1) * limit
  const filter = {
    ...(chatRoom !== '' && {
      chatRoom: mongoose.Types.ObjectId(chatRoom)
    }),
    ...(email && { $or: [{ email }, { to: email }, { to: { $size: 0 } }] })
  }

  ChatModel.countDocuments(filter)
    .then((filterRecord) => {
      const totalRows = filterRecord
      ChatModel.aggregate([
        { $match: filter },
        { $sort: { createdAt: -1 } },
        { $skip: skip },
        { $limit: limit },
        { $sort: { createdAt: 1 } }
      ])
        .exec()
        .then((results) => {
          res.json({
            item: results,
            totalRows
          })
        })
        .catch((err) => {
          responseService.resultResponse({
            type: 'error',
            status: 500,
            data: err,
            res
          })
        })
    })
    .catch(err =>
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    )
}

/**
 * UPDATE ITEM
 *
 * @description :: UPDATE ONE ITEM IN DB
 */

chatController.update = (req, res) => {
  const { _id } = req.params
  dbService.updateOne(
    ChatModel,
    {
      _id
    },
    req.body,
    res
  )
}

/**
 * DELETE ITEM
 *
 * @description :: DELETE ONE ITEM IN DB
 */

chatController.delete = (req, res) => {
  const { _id } = req.params
  dbService.deletePhysical(
    ChatModel,
    {
      _id
    },
    res
  )
}

module.exports = chatController
