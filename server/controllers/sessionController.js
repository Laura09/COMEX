const Session = require('../models/sessionModel')
const dbService = require('../services/dbService')
const sessionController = {}

/**
 * GET ALL SESSIONS
 *
 * @description :: list of all sessions in db
 */

sessionController.getAll = (req, res) => {
  const body = req.body
  let filter = {}
  if (body.search && body.search !== '') {
    const search = new RegExp(body.search.trim(), 'i')
    filter = {
      $or: [
        {
          collectionName: search
        },
        {
          action: search
        },
        {
          email: search
        }
      ]
    }
  }
  dbService.getAllPaginate(Session, filter, req, res, 'user')
}

/**
 * DELETE SESSION
 *
 * @description :: DELETE USER SESSION IN DB
 */

sessionController.delete = (req, res) => {
  const { _id } = req.params
  dbService.deletePhysical(
    Session,
    {
      _id
    },
    res
  )
}

module.exports = sessionController
