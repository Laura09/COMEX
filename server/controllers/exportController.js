const moment = require('moment')
const async = require('async')
const driveService = require('../services/driveService')
const FileModel = require('../models/fileModel')
const exportModel = require('../models/exportModel')
const dbService = require('../services/dbService')
const responseService = require('../services/responseService')
const fileController = require('./fileController')

const exportController = {}
/**
 * CREATE
 *
 * @description :: CREATE ITEM IN DB
 */
exportController.create = (req, res) => {
  const data = { ...req.body, user: req.user }
  const { exportDate } = data
  if (exportDate) {
    data.exportDate = new Date(
      moment(exportDate, 'DD-MM-YYYY').format('YYYY-MM-DD')
    )
  }
  dbService.createOneSession(exportModel, 'Export', data, req, res, true)
}

/**
 * UPDATE ITEM
 *
 * @description :: UPDATE ONE ITEM IN DB
 */

exportController.update = (req, res) => {
  const { _id } = req.params
  const data = req.body
  const { exportDate, exportProcessDate } = data
  if (exportDate) {
    data.exportDate = new Date(
      moment(exportDate, 'DD-MM-YYYY').format('YYYY-MM-DD')
    )
  }
  if (exportProcessDate) {
    data.exportProcessDate = new Date(moment(exportProcessDate).utc())
  }
  dbService.updateOneSession(
    exportModel,
    'Export',
    {
      _id
    },
    data,
    req,
    res
  )
}
/**
 * GET ALL
 *
 * @description :: list of all items in db
 */

exportController.getAll = (req, res) => {
  const body = req.body
  const {
    search,
    brand,
    userBrands,
    originCountry,
    destinationCountry,
    startDate,
    endDate,
    status,
    orderStatus,
    sortDirection,
    sortBy
  } = body

  const currentPage = Number(body.currentPage)
  const limit = Number(body.perPage) || 50
  const skip = (currentPage - 1) * limit
  const startDateFormat = startDate
    ? new Date(
      moment(startDate, 'DD-MM-YYYY')
        .startOf('day')
        .utc()
        .format('YYYY-MM-DD')
    )
    : null

  const endDateFormat = endDate
    ? new Date(
      moment(endDate, 'DD-MM-YYYY').endOf('day').utc().format('YYYY-MM-DD')
    )
    : null

  const searchName =
    search && search !== '' ? new RegExp(search.trim(), 'i') : ''
  const filter = {
    ...(searchName !== '' && {
      $or: [
        {
          exportNumber: searchName
        },
        {
          status: searchName
        },
        {
          orderStatus: searchName
        }
      ]
    }),
    ...(userBrands && userBrands.length > 0 &&
      {
        brand: {
          $in: userBrands
        }
      }),
    ...(brand &&
      {
        brand
      }),
    ...(originCountry &&
      {
        originCountry
      }),
    ...(destinationCountry &&
      {
        destinationCountry
      }),
    ...(status && { status }),
    ...(orderStatus && { orderStatus }),
    ...(startDateFormat && !endDateFormat &&
      {
        exportDate: {
          $gte: startDateFormat
        }
      }
    ),
    ...(endDateFormat && !startDateFormat &&
      {
        exportDate: {
          $lte: endDateFormat
        }
      }
    ),
    ...(startDateFormat && endDateFormat &&
      {
        exportDate: {
          $gte: startDateFormat,
          $lte: endDateFormat
        }
      }
    )
  }

  exportModel
    .countDocuments(filter)
    .then((filterRecord) => {
      const totalRows = filterRecord
      exportModel
        .find(filter)
        .sort({
          [sortBy]: sortDirection
        })
        .skip(skip)
        .limit(limit)
        .populate([
          {
            path: 'originCountry'
          },
          {
            path: 'destinationCountry'
          },
          {
            path: 'brand'
          }
        ])
        .lean()
        .exec()
        .then((results) => {
          async.each(
            results,
            async (exportFind) => {
              const filesWithError =
                await fileController.getFilesWithErrorsFunction({
                  exportID: exportFind._id
                })
              if (
                filesWithError &&
                (filesWithError.empty || filesWithError.files.length > 0)
              ) {
                exportFind.incorrect = true
                exportFind.filesWithError = filesWithError.filesWithErrors
              }
              if (filesWithError) {
                exportFind.empty = filesWithError.empty
              }
            },
            (err) => {
              // if any of the file processing produced an error, err would equal that error
              if (err) {
                console.log('A file failed to process')
              } else {
                res.json({
                  item: results,
                  totalRows
                })
              }
            }
          )
        })
        .catch((err) => {
          responseService.resultResponse({
            type: 'error',
            status: 500,
            data: err,
            res
          })
        })
    })
    .catch((err) => {
      responseService.resultResponse({
        type: 'error',
        status: 500,
        data: err,
        res
      })
    })
}

/**
 * GET ONE
 *
 * @description :: GET AN ITEM FROM DB
 */
exportController.getOne = (req, res) => {
  const { _id } = req.params
  dbService.getOnePopulate(
    exportModel,
    {
      _id
    },
    [
      {
        path: 'originCountry'
      },
      {
        path: 'destinationCountry'
      },
      {
        path: 'brand'
      }
    ],
    res
  )
}
/**
 * DELETE ITEM
 *
 * @description :: DELETE ONE ITEM IN DB
 */

exportController.delete = async (req, res) => {
  const { _id } = req.params
  try {
    const files = await FileModel.find({ exportID: _id }, '_id fileId ')
      .lean()
      .exec()
    if (files.length > 0) {
      for (const file of files) {
        const deleteFile = await driveService.delete(file.fileId)
        if (deleteFile) {
          dbService.deletePhysicalSession(
            FileModel,
            'File',
            {
              _id: file._id
            },
            req,
            res,
            false
          )
        }
      }
    }
    dbService.deletePhysicalSession(
      exportModel,
      'exportModel',
      {
        _id
      },
      req,
      res
    )
  } catch (err) {
    responseService.resultResponse({
      type: 'error',
      status: 500,
      data: err,
      res
    })
  }
}

module.exports = exportController
