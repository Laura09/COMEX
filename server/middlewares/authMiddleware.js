const jwt = require('jsonwebtoken')
const User = require('../models/userModel')
const Privilege = require('../models/privilegeModel')

//= =====================
// VERIFICAR TOKEN
// ======================

const verifyToken = (req, res, next) => {
  const token = req.headers.authorization
    ? req.headers.authorization.split(' ')[1]
    : '' // AUthorization
  jwt.verify(token, process.env.SEED, (err, decode) => {
    if (err) {
      return res.status(401).json({
        ok: false,
        err: {
          message: 'Token no valido'
        }
      })
    }
    User.findOne({
      email: decode.user.email
    })
      .populate('role')
      .lean()
      .exec()
      .then((user) => {
        // if no user is found, return the message
        if (!user) {
          return res.status(400).json({
            ok: false,
            err: {
              message: 'Usuario Incorrecto'
            }
          })
        } else {
          Privilege.find({
            'role._id': user.role
          })
            .sort({ position: 1 })
            .populate({
              path: 'children._id',
              match: {
                'role._id': user.role
              },
              transform (doc) {
                if (doc) {
                  return doc
                }
              },
              populate: {
                path: 'children._id',
                match: {
                  'role._id': user.role
                },
                transform (doc) {
                  if (doc) {
                    return doc
                  }
                }
              }
            })
            .lean()
            .exec()
            .then((modelFind) => {
              user.privileges = modelFind.filter(x => !x.parent)
              user.access = [...modelFind]
              req.user = user
              next()
            })
            .catch(() => {
              req.user = user
              next()
            })
        }
      })
      .catch((err) => {
        return res.status(500).json({
          ok: false,
          err
        })
      })
  })
}

module.exports = {
  verifyToken
}
